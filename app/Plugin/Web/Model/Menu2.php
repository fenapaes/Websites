<?php

class Menu2 extends WebAppModel {

	public $useTable = 'menu2';
	public $primaryKey = 'me2_id';
	//public $useDbConfig = 'Apaebrasil';
	public $useDbConfig = 'portalReader';
	
	public $hasMany = array(
		'Menu3' => array(
			'className' => 'Web.Menu3',
			'foreignKey' => 'me3_me2_id'
		)
	);
	
}