<?php

class Anexo extends WebAppModel {

	public $useTable = 'anexo_arquivo';
	public $primaryKey = 'ana_id';
	public $useDbConfig = 'portalReader';
	
	public $belongsTo = array(
		'Entidade' => array(
			'className' => 'Web.Entidade',
			'foreignKey' => 'ana_ent_id'
		),
		'Noticia' => array(
			'className' => 'Web.Noticia',
			'foreignKey' => 'ana_mod_id'
		)
	);
	
	
	/*
	public function afterFind($results, $primary = false) {
		if (is_array( $results )) {
		foreach($results as $key => $value) {
			if ( isset($value['Arquivo']['arq_data']) ) {
				$results[$key]['Arquivo']['arq_data'] = date('Y-m-d\TH:i:s.000\Z', strtotime( $value['Arquivo']['arq_data'] ) );
			}
		}
		}
		return $results;
	}
	*/

}