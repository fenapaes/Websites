<?php

class Noticia extends WebAppModel {

	public $useTable = 'noticia';
	public $primaryKey = 'not_id';
	public $useDbConfig = 'portalReader';
	
	public $hasMany = array(
		'Anexo' => array(
			'className' => 'Web.Anexo',
			'foreignKey' => 'ana_mod_id',
			'conditions' => array(
				'Anexo.ana_mod' => 'noticia'
			)
		),
		'Album' => array(
			'className' => 'Web.Foto',
			'foreignKey' => 'fot_mod_id',
			'conditions' => array(
				'Album.fot_mod' => 'noticia'
			)
		)
	);
	
	public function afterFind($results, $primary = false) {
		if (is_array( $results )) {
		foreach($results as $key => $value) {
			if ( isset($value['Noticia']['not_data']) ) {
				$results[$key]['Noticia']['not_data'] = date('Y-m-d\TH:i:s.000\-03:00', strtotime( $value['Noticia']['not_data'] ) );
			}
			if ( isset($value['Noticia']['not_data_atualizado']) ) {
				$results[$key]['Noticia']['not_data_atualizado'] = date('Y-m-d\TH:i:s.000\-03:00', strtotime( $value['Noticia']['not_data_atualizado'] ) );
			}
		}
		}
		return $results;
	}

}