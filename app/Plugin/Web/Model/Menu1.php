<?php

class Menu1 extends WebAppModel {

	public $useTable = 'menu';
	public $primaryKey = 'men_id';
	//public $useDbConfig = 'Apaebrasil';
	public $useDbConfig = 'portalReader';
	
	public $hasMany = array(
		'Menu2' => array(
			'className' => 'Web.Menu2',
			'foreignKey' => 'me2_men_id'
		)
	);

	
}