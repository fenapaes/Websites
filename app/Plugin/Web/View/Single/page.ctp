<div class="container" style="min-height: 650px;">
		
	<div class="row">
		<ng-include src="'/web/app/common/social.html'"></ng-include>
	</div>
	<br>

	<div class="row">
		<div class="col-md-3">
			<form class="form-inline" role="form">
				<div class="form-group">
					<label class="sr-only" for="exampleInputEmail2">Buscar</label>
					<div class="input-group">
						<input type="text" class="form-control" id="buscar" placeholder="Pesquise...">
						<span class="input-group-btn">
							<button class="btn btn-default" type="button">Ir!</button>
						</span>
					</div>
				</div>
			</form>
		</div>
		 <div class="col-md-9">
			<div class="btn-group pull-right">
				<a href="#/" class="btn btn-default"><i class="fa fa-home"></i></a>
				<a href="#/oquefazemos" class="btn btn-success">O que fazemos ?</a>
				<a href="#/apaesnosestados" class="btn btn-primary">Apaes nos Estados</a>
				<a href="#/faleconosco" class="btn btn-warning">Fale Conosco</a>
				<a href="#/comoajudar" class="btn btn-info">Saiba como Ajudar</a>
				<a href="#/artigo/47" class="btn btn-danger">Apae em Rede</a>
			 </div>
		</div>
	</div>
	<br>
	<div class="row">
		<div class="col-md-12 sidebar-offcanvas" id="sidebar" role="navigation">
			<nav class="navbar navbar-default" role="navigation">
				<div class="container-fluid">
					<div class="navbar-header">
						<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
					</div>
					
					<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
						<ul class="nav navbar-nav" id="main-menu">
							<li ng-repeat="menu in topMenus">
								<a ng-href="{{menu.Menu.men_link}}">{{menu.Menu.men_titulo}}</a>
								<ul ng-if="menu.Submenu1.length > 0" class="dropdown-menu">
									<li ng-repeat="sub1 in menu.Submenu1">
										<a ng-href="{{sub1.men_link}}">{{sub1.men_titulo}}</a>
										<ul ng-if="sub1.Submenu2.length > 0" class="dropdown-menu">
											<li ng-repeat="sub2 in sub1.Submenu2">
												<a ng-href="{{sub2.men_link}}">{{sub2.men_titulo}}</a>
											</li>
										</ul>
									</li>
								</ul>
							</li>
						</ul>
					</div>
					
				</div>
			</nav>
		</div>
	</div>
	
	<div class="view-animate-container" style="min-height: 400px;">
		<div ng-view class="view-animate"></div>
	</div>
</div>
<div class="container-fluid" style="margin-top: 10px;">
	<ng-include src="'/web/app/includes/bottomMenu.html'"></ng-include>
	<ng-include src="'/web/app/common/endereco.html'"></ng-include>
</div>