fenapaesApp.config(function($routeProvider) {
	appVersion = '?ver=1.0.1';
	$routeProvider
	.when('/', {
		controller: 'homeCtrl', 
		templateUrl: '/web/app/home/home.html'+appVersion
	})
	.when('/apaesnosestados', {
		controller: 'mapsController',
		templateUrl: '/web/app/static/apaesnosestados.html'+appVersion
	})
	.when('/comoajudar', {
		templateUrl: '/web/app/static/comoajudar.html'+appVersion
	})
		.when('/faleconosco', {
		templateUrl: '/web/app/static/faleconosco.html'+appVersion
	})
	.when('/oquefazemos', {
		templateUrl: '/web/app/static/oquefazemos.html'+appVersion
	})
	.when('/noticia', {
		controller: 'noticiaCtrl', 
		templateUrl: '/web/app/noticia/index.html'+appVersion
	})
	.when('/noticias/:year', {
		controller: 'noticiaYearCtrl', 
		templateUrl: '/web/app/noticia/index.html'+appVersion
	})
	.when('/noticia/:id', {
		controller: 'noticiaViewCtrl', 
		templateUrl: '/web/app/noticia/view.html'+appVersion
	})
	.when('/artigo/:id', {
		controller: 'artigoViewCtrl', 
		templateUrl: '/web/app/artigo/view.html'+appVersion
	})
	.when('/arquivo/list/:id', {
		controller: 'arquivoListCtrl', 
		templateUrl: '/web/app/arquivo/list.html'+appVersion
	})
	.when('/arquivo/:id', {
		controller: 'arquivoDownloadCtrl',
		templateUrl: '/web/app/arquivo/download.html'+appVersion
	})
	.when('/foto.phtml/:id/:size', {
		controller: 'fotoCtrl', 
	})
	
	.otherwise({
		templateUrl: '/web/app/errors/404.html'+appVersion
	});
	
});