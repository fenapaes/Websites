var fenapaesApp = angular.module('fenapaesApp', 
	['ngRoute',
	'ngResource',
	'classy', 
	'ui.bootstrap',
	'dialogs.main',
	'classy-on',
	'ngMaps',
	'angulike',
	'ngSanitize'
	//'angulartics',
	//'angulartics.google.analytics'
	]);

fenapaesApp.run([
	'$rootScope', function ($rootScope) {
		$rootScope.facebookAppId = '451419348279152'; // set your facebook app id here
	}
]);

fenapaesApp.run(function ($rootScope, $location) {
    $rootScope.$on('$routeChangeSuccess', function(){
        ga('send', 'pageview', $location.path());
    });
});

var regexIso8601 = /^(\d{4})-(\d{2})-(\d{2})T(\d{2}):(\d{2}):(\d{2})\.(\d{3})\Z$/;

function convertDateStringsToDates(input) {
	// Ignore things that aren't objects.
	if (typeof input !== "object") return input;
	
	for (var key in input) {
		if (!input.hasOwnProperty(key)) continue;
	
		var value = input[key];
		var match;
		// Check for string properties which look like dates.
		if (typeof value === "string" && (match = value.match(regexIso8601))) {
			var milliseconds = Date.parse(match[0]);
			if (!isNaN(milliseconds)) {
				input[key] = new Date(milliseconds);
			}
		} else if (typeof value === "object") {
			// Recurse into object
			convertDateStringsToDates(value);
		}
	}
}

fenapaesApp.config(["$httpProvider", function ($httpProvider) {
	$httpProvider.defaults.transformResponse.push(function(responseData){
		convertDateStringsToDates(responseData);
		return responseData;
	});
}]);