fenapaesApp.service('DataService', function(){
	return {
		appVersion: '1.0.0',
		
		handleLinks: function(html) {
				
			parsed = $(html); // jQuery parseHTML
			
			parsed = this.parseArqImg(parsed);
			
			parsed = this.parseArqLink(parsed);
			
			parsed = this.parseAneLink(parsed);
			
			parsed = this.parseArqList(parsed);
			
			parsed = this.parseArtLink(parsed);
			
			newHtml = '';
			parsed.each(function(i, node){
				//console.log($(node).context);
				newHtml+= ($(node).context.outerHTML)?$(node).context.outerHTML:'';
			});
			
			return newHtml;
		},
		
		parseArqImg: function(parsed) {
			
			arq_imgs = parsed.find('img[src*="arquivo.phtml?a="]');
			
			arq_imgs.each(function(img){
				src = $(arq_imgs[img]).attr('src');
				src = src.split('=');
				nsrc = '/arquivo/'+src[src.length-1];
				parsed.find(arq_imgs[img]).attr('src',nsrc);
			});
			return parsed;
		},
		
		parseArqLink: function(parsed) {
			
			arq_links = parsed.find('a[href*="arquivo.phtml?a="]');
			
			arq_links.each(function(link){
				href = $(arq_links[link]).attr('href');
				href = href.split('=');
				nhref = '/arquivo/'+href[href.length-1];
				parsed.find(arq_links[link]).attr('href',nhref);
			});
			return parsed;
		},
		
		parseAneLink: function(parsed) {
			
			arq_links = parsed.find('a[href*="anexo.phtml"]');
			
			arq_links.each(function(link){
				href = $(arq_links[link]).attr('href');
				href = href.split('/');
				nhref = '/anexo/'+href[href.length-1];
				parsed.find(arq_links[link]).attr('href',nhref);
			});
			return parsed;
		},
		
		parseArqList: function(parsed) {
			
			arq_links = parsed.find('a[href*="arquivos.phtml?t="]');
			
			arq_links.each(function(link){
				href = $(arq_links[link]).attr('href');
				href = href.split('=');
				nhref = '#/arquivo/list/'+href[href.length-1];
				parsed.find(arq_links[link]).attr('href',nhref);
			});
			return parsed;
		},
		
		parseArtLink: function(parsed) {
		
			art_links = parsed.find('a[href*="artigo.phtml?a="]');
			art_links.each(function(link){
				href = $(art_links[link]).attr('href');
				href = href.split('=');
				nhref = '#/artigo/'+href[href.length-1];
				parsed.find(art_links[link]).attr('href',nhref);
			});
			return parsed;
		}
	
	};
});
