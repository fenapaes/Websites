fenapaesApp.cC({
	name: 'mapsController', 
	inject: ['$scope','$resource','dialogs','$sce','DataService','modelService'],
	init: function() {
		this.Estado = this.modelService.model.Estado;
		this.Entidade = this.modelService.model.Entidade;
		this._loadEstados();
		this.$.map = {
			center: [-15, -49],
			options: function() {
				return {
					streetViewControl: false,
					scrollwheel: false
				}
			}
		};
	},
	methods: {
		_loadEstados: function() {
			this.Estado.get(
				{
					limit: 50
				}).then(function(data) {
					coords = [];
					properties = [];
					titles = [];
					data.data.forEach(function(item){
						coords.push([item.Estado.latitude, item.Estado.longitude]);
						properties.push(item.Estado.sigla);
						titles.push(item.Estado.sigla);
					}.bind(this));
					this.$.points = {
						coords: coords,
						properties: properties,
						options: function(coords, properties, map, i) {
							return {
								title: titles[i]
							};
						},
						events: {
							click: function(e, point, map, points) {
								sigla = properties[points.indexOf(point)];
								this.Entidade.get(
									{
										q: 'Entidade.ent_fed_uf.eq.'+sigla,
										limit: 1000,
										sort: 'ent_id'
									}
								).then(function(data){
									if (data.data[0].Entidade.ent_link.split('.').length == 1) {
										data.data[0].Entidade.ent_link += '.apaebrasil.org.br';
									}
									this.$.entSelected = data.data[0];
									if (sigla == 'DF') {
										this.$.apaeDFSelected = data.data[1];
									} else {
										delete(this.$.apaeDFSelected);
									}
									
									this.$.Entidades = data.data;
								}.bind(this));
							}.bind(this)
						}
					};
			}.bind(this));
		}
	}
});