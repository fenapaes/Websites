fenapaesApp.cC({
	name: 'rybenaController',
	inject: ['$scope','$resource','DataService','$rootScope','$location'],
	init: function() {
		locSearch = this.$location.search();

		if (locSearch.rybena == 'apae123') {
			this.$.rybenaActive = true;
		} else {
			this.$.rybenaActive = false;
		}

		this.rybenaRepositoryUrl = "http://www.rybena.com.br/RybenaRepository/";
		//this.serverUrl = "http://www.rybena.com.br/RybenaServer/CodeApplication";
		this.serverUrl = "/web/app/rybena/server.js";
		this.scriptRybenaLoaded = 0;

		//console.log('rybena controller');
		//rybena('areaRybenaLibras','areaRybenaTTS');
		//includeRybena();
	},
	methods: {
		startVox: function() {
			console.log('init vox');
			if (!this.scriptRybenaLoaded) {
				$.getScript(
					this.serverUrl,
					function() {
						console.log();
						this.scriptRybenaLoaded = 1;
						if (document.getElementById("loadingDiv") == null) {
							var loadingDiv = document.createElement("div");
							loadingDiv.id = "loadingDiv";
							loadingDiv.style.cssText = "width: 120px; height: 120px; position: absolute; top: 0px; left: 120px; z-index:9999;";
						} else {
							var loadingDiv = document.getElementById("loadingDiv");
							window.document.getElementById("loadingDiv").style.visibility = "visible";
						}
						loadingDiv.innerHTML =
							'<img width="120" height="120" src="'
							+ this.rybenaRepositoryUrl
							+ 'resource/img/loading.gif"></img>';
						window.document.getElementById("rybenaVozDiv").appendChild(loadingDiv);
						window.load();
						window.initialize();
						window.showTTS();
					}.bind(this)
				);
			} else {
				window.showTTS();
			}
		},
		startLibras: function() {
			if (!this.scriptRybenaLoaded) {
				$.getScript(
					this.serverUrl,
					function() {
						this.scriptRybenaLoaded = 1;
						if (window.document.getElementById("loadingDiv") == null) {
							var loadingDiv = window.document.createElement("div");
							loadingDiv.id = "loadingDiv";
							loadingDiv.style.cssText = "width: 120px; height: 120px; position: absolute; top: 0px; left: 120px; z-index:9999;";
						} else {
							var loadingDiv = document.getElementById("loadingDiv");
							window.document.getElementById("loadingDiv").style.visibility = "visible";
						}
						loadingDiv.innerHTML =
							'<img width="120" height="120" src="'
							+ this.rybenaRepositoryUrl
							+ 'resource/img/loading.gif"></img>';
						window.document.getElementById("rybenaVozDiv").appendChild(loadingDiv);
						load();
						initialize();
						showRybena();
					}.bind(this)
				);
			} else {
				showRybena();
			}
		},
	}
});
