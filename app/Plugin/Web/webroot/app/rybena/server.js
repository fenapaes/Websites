var code = '6e1e9f7fb3cf1ff78669ba43c63fe1f292663527cd8d19d2052054e7d89bad03';
var rybenaVisible = false;
var ttsVisible = false;
var lastText = null;
var countOpensRybena = 0;
var countOpensRybenaVoz = 0;
var audioType = "wav";
var playRybenaTextTimeout;
var playTimeout;
var internetExplorerVersion = -1;// Return value assumes failure.

var rybenaPessoalMode = false;

function ctsLoad(url, callback) {
	var xhr;

	if (typeof XMLHttpRequest !== 'undefined') {

		xhr = new XMLHttpRequest();

	} else {
		var versions = [ "MSXML2.XmlHttp.5.0", "MSXML2.XmlHttp.4.0",
				"MSXML2.XmlHttp.3.0", "MSXML2.XmlHttp.2.0", "Microsoft.XmlHttp" ];

		for ( var i = 0, len = versions.length; i < len; i++) {
			try {
				xhr = new ActiveXObject(versions[i]);
				break;
			} catch (e) {
				alert(e);
			}
		}
	}

	xhr.onload = function ensureReadiness() {
		if (xhr.readyState < 4) {
			return;
		}

		if (xhr.status == 204) {
			window.location.reload();
		}

		if (xhr.status !== 200) {
			return;
		}

		if (xhr.readyState === 4) {
			callback(xhr);
		}
	};

	xhr.open('GET', url, true);
	xhr.send('');
}

var playStatus = 'stopped';
var wordArray = new Array();
var playSpeed = 90;

var serverUrl = "https://www.rybena.com.br/RybenaServer/"; // Cuidado nao alterar aqui!
// Variavel setada em rybena.xml
var webrUrl = "https://www.rybena.com.br/RybenaRepository/"; // Cuidado nao alterar aqui! Variavel
// setada em rybena.xml

var count = 0;
var played = 0;
var translateArray = new Array();
var searchCount = 0;
var buscando = false;
var emTraducao = false;
var legend = "Ryben&aacute;";
var staticWordArray = new Array(30);
var dynamicWordArray = new Array(50);
var totVariaveis = 0;
var translateImg;
var stopedImg;
var appleText = "";
var text = "";

// Variaveis utilizadas na paginacao do TTS
var useTTSPagination = false;
var currentPlay = 0;
var currentDownload = 0;
var tts_pagination_timeout = null;
var phraseArray = new Array();
var startDownload = true;
var startDownload1 = false;
var sourceLouded = false;
var sourceLouded1 = false;

// Variaveis utilizadas na paginação do libras
var phraseArrayLibras = new Array();
var translateCount = 0;

function loadImages() {
	var stopedUrl = webrUrl + 'resource/img/rybeninha_idle.gif';
	var translateUrl = webrUrl + 'resource/img/rybeninha_loading.gif';

	ctsLoad(stopedUrl, function(xhr) {
		stopedImg = xhr.responseText;
	});

	ctsLoad(translateUrl, function(xhr) {
		translateImg = xhr.responseText;
	});
}

function getWord(id) {
	for ( var int = 0; int < staticWordArray.length; int++) {
		var obj = staticWordArray[int];
		if (obj != null && obj.id == id) {
			return obj.word;
		}
	}
	for ( var int = 0; int < dynamicWordArray.length; int++) {
		var obj = dynamicWordArray[int];
		if (obj != null && obj.id == id) {
			return obj.word;
		}
	}
	return -1;
}

function setDynamicWord(id, word) {
	if (totVariaveis >= 50) {
		totVariaveis = 0;
	}
	var obj = new Object();
	obj.id = id;
	obj.word = word;
	dynamicWordArray[totVariaveis++] = obj;
}

function getIframeSelectionText(iframe) {
	if (iframe) {
		var win = iframe.contentWindow;
		var doc = win.document;

		if (win.getSelection) {
			return win.getSelection().toString();
		} else if (doc.selection && doc.selection.createRange) {
			return doc.selection.createRange().text;
		}
	} else {
		return "";
	}
}

function replaceAll(find, replace, str) {
	return str.replace(new RegExp(find, 'g'), replace);
}

function getSelectedText() {

	if (document.getElementById("rybenaDiv").style.visibility != "hidden"
			|| document.getElementById("rybenaVozDiv").style.visibility != "hidden") {
		if (window.getSelection) {
			text = "" + window.getSelection();
			// console.log("window.getSelection() text = "+text);
		} else if (document.getSelection) {
			text = "" + document.getSelection();
			// console.log("document.getSelection() text = "+text);
		} else if (document.selection) {
			text = "" + document.selection.createRange().text;
			// console.log("document.selection.createRange() text = "+text);
		} else if (iframe) {
			text = getIframeSelectionText(iframe);
		}

		if (text == "" || text == null) {
			return;
		}

		if ((navigator.userAgent.indexOf('iPhone') != -1)
				|| (navigator.userAgent.indexOf('iPod') != -1)) {
			addEvent(window.document, 'select', function getAppleText() {
				appleText = getSelectedText();
			});
		}

		if (document.getElementById("rybenaDiv").style.visibility != "hidden"
				&& document.getElementById("rybenaVozDiv").style.visibility == "hidden"
				&& !(playStatus == "paused")) {

			// IE>=10
			var iever = getInternetExplorerVersion();
			if (iever > -1) {
				if (iever < 10) { // If user is running //
					window.document.getElementById("rybenaDiv").style.visibility = "visible";
					document.getElementById("rybenaLabel").innerHTML = 'Atualize seu browser.';
					return null;
				} // Caso seja maior que 10 ok pode rodar...
			}

			stopRybena();
			traduzindo();

			if (text != " " && text != "  ") {
				if (playStatus == 'searching' || playStatus == 'playing') {
					// return;
					stopRybena();
					wordArray = new Array();
				}
				console.log('Chamando playRybenaText()');
				playRybenaText(text);
			}

			// clearSelection();

		} else if (document.getElementById("rybenaDiv").style.visibility == "hidden"
				&& document.getElementById("rybenaVozDiv").style.visibility != "hidden") {

			// IE>=10
			var iever = getInternetExplorerVersion();
			if (iever > -1) {
				if (iever < 10) { // If user is running //
					window.document.getElementById("rybenaDiv").style.visibility = "visible";
					document.getElementById("rybenaLabel").innerHTML = 'Atualize seu browser.';
					return null;
				} // Caso seja maior que 10 ok pode rodar...
			}

			stopRybena();
			if (text != " " && text != "  ") {
				//
				document.getElementById('playBtn').style.visibility = 'hidden';
				if (!useTTSPagination) {
					playTtsText(text);
				} else {
					console
							.log("Verificando se os audioElements estão parados");
					if (!window.document.getElementById("tts").paused
							|| !window.document.getElementById("tts1").paused) {
						console.log("Pausando AudioElements!!!!!");
						pauseTTS();
					}
					console.log("Chamando triggerTTSPaginated");
					triggerTTSPaginated(text);
				}
			}
			playStatus = "playing";
			// clearSelection();

		}

		return text;
	} else {
		return null;
	}
}

function getInternetExplorerVersion()
// Returns the version of Internet Explorer or a -1
// (indicating the use of another browser).
{
	var ua = navigator.userAgent;
	if (navigator.appName == 'Microsoft Internet Explorer') {
		var re = new RegExp("MSIE ([0-9]{1,}[\.0-9]{0,})");
		if (re.exec(ua) != null) {
			internetExplorerVersion = parseFloat(RegExp.$1);
		}
	} else {
		var reFirefox = new RegExp("Firefox/([0-9]{1,}[\.0-9]{0,})");
		if (reFirefox.exec(ua) != null) {
			console.log('Firefox Versao: ' + parseFloat(RegExp.$1));
			return internetExplorerVersion;
		} else {
			var reIE11 = new RegExp("rv:([0-9]{1,}[\.0-9]{0,})");
			if (reIE11.exec(ua) != null) {
				internetExplorerVersion = parseFloat(RegExp.$1);
			}
		}
	}
	console.log("Microsoft Internet Explorer Versao: "
			+ internetExplorerVersion);
	return internetExplorerVersion;
}

function clearSelection() {

	//console.log("Clear Selection Called! ");

	var sel;
	if ((sel = document.selection) && sel.empty) {
		sel.empty();
	} else {
		if (window.getSelection) {
			window.getSelection().removeAllRanges();
		}
		var activeEl = document.activeElement;
		if (activeEl) {
			var tagName = activeEl.nodeName.toLowerCase();
			if (tagName == "textarea"
					|| (tagName == "input" && activeEl.type == "text")) {
				// Collapse the selection to the end
				activeEl.selectionStart = activeEl.selectionEnd;
			}
		}
	}
}

function playRybena() {

	console.log("Entering Play Rybena...");

	if ((navigator.userAgent.indexOf('iPhone') != -1)
			|| (navigator.userAgent.indexOf('iPod') != -1)) {
		texto = appleText;
	} else {
		texto = getSelectedText();
	}

	if (texto == null || texto == "") {
		texto = "marcar texto";
	}

	texto = texto.replace("%", "porcento");

	console.log("Calling Play Rybena Text" + texto);

	playRybenaText(texto);

}

function playRybenaText(texto) {

	if (texto == null || texto == "") {
		texto = "selecionar um texto";
	}

	if (playStatus == 'stopped' && emTraducao == false) {

		// console.log("Texto antes do encode: ", texto);

		phraseArrayLibras = cleanArray(replaceAll(

				"\"",
				"",
				replaceAll("\'", "", replaceAll("”", "", replaceAll("“", "",
						replaceAll("‘", "", replaceAll("’", "", texto))))))
				.split(/\n/));
		translateCount = 0;
		var aux1 = phraseArrayLibras.length - 1;
		console.log('Iniciando tradução[' + translateCount + '] de [' + aux1
				+ '] txt[' + phraseArrayLibras[translateCount] + ']');
		var rybenaUrl = serverUrl
				+ 'RybenaApplication/translate/'
				+ replaceAll("\\+", "==CTSPLUS==", replaceAll('/',
						"==CTSSLASH==", base64_encode(phraseArrayLibras[0])))
				+ "/" + code + "/" + translateCount;

		// console.log("Texto depois do encode: ",
		// base64_decode(base64_encode(texto)), "[", base64_encode(texto),
		// "]");

		delete wordArray;
		wordArray = new Array();
		playStatus = "starting";
		count = 0;
		played = 0;
		searchCount = 0;
		buscando = false;
		emTraducao = true;
		ctsLoad(rybenaUrl, function(xhr) {
			emTraducao = false;
			var translate = xhr.responseText;
			translateArray = translate.split(";");
			if (translateArray.length > 1) {
				console.log('Terminou a requisição de tradução['
						+ translateCount + ']');
				translateCount = translateCount + 1;
				playStatus = 'searching';
				playRybenaText(texto);
			} else {
				stopRybena();
			}
		});

	}

	if (playStatus == 'paused') {
		playRybenaTextTimeout = setTimeout(function() {
			playRybenaText(texto);
		}, 100);
		return;
	}

	if (playStatus == 'searching' || playStatus == 'playing') {
		if (searchCount < translateArray.length) {
			if (buscando === false) {
				var imageCode = translateArray[searchCount++];
				var imageDesc = translateArray[searchCount++];
				var word = getWord(imageCode);
				if (word != -1) {
					word.imageDesc = imageDesc;
					addWordArray(word);
				} else {
					if (imageCode > 0) {
						buscando = true;
						rybenaImageSearch(imageCode, imageDesc);
					}
				}
			}
			playRybenaTextTimeout = setTimeout(function() {
				playRybenaText(texto);
			}, 100);
		} else {
			var aux = translateCount - 1;
			console.log('Terminou de baixar as imagens para a tradução[' + aux
					+ ']');
			if (translateCount < phraseArrayLibras.length) {
				var rybenaUrl = serverUrl
						+ 'RybenaApplication/translate/'
						+ replaceAll(
								"\\+",
								"==CTSPLUS==",
								replaceAll(
										'/',
										"==CTSSLASH==",
										base64_encode(phraseArrayLibras[translateCount])))
						+ "/" + code + "/" + translateCount;
				var aux2 = phraseArrayLibras.length - 1;
				console.log('Iniciando tradução[' + translateCount + '] de ['
						+ aux2 + '] txt[' + phraseArrayLibras[translateCount]
						+ ']');
				emTraducao = true;
				ctsLoad(rybenaUrl, function(xhr) {
					emTraducao = false;
					var translate = xhr.responseText;
					translateArray = new Array();
					translateArray = translate.split(";");
					if (translateArray.length > 1) {
						console.log('Terminou a requisição de tradução['
								+ translateCount + ']');
						translateCount = translateCount + 1;
						searchCount = 0;
						if (playStatus != "stopped") {
							playRybenaText(texto);
						}
					} else {
						stopRybena();
					}
				});
			}
		}
	}

	if ((searchCount >= 6 || searchCount >= translateArray.length)
			&& playStatus == 'searching') {
		playStatus = "playing";
		playCTS();
	}
	// console.log('Leaving Play Rybena Text...');

}

function playTxt(texto, t, callback) {

	if (texto == null || texto == "") {
		console.log("Texto null");
	}

	//console.log("playStatus: "+playStatus);
	if(playStatus == 'stopping' || playStatus == 'stopped'){
		//console.log("entrou aqui wordArray.length: "+wordArray.length+" played: "+played+" searchCount: "+searchCount);
		if(wordArray.length == played && searchCount > 0){
			//console.log("terminou");
			if (callback && typeof(callback) === "function") {
				console.log("Terminou de sinalizar a traducao chamando callback!");
				played = 0;
				searchCount = 0;
				callback();
				return;
			}
		}
	}

	if (playStatus == 'stopped' && emTraducao == false) {

		//playSpeed = 60;
		var tempo = t.split(" --> ");
		var temp = toMilliseconds(tempo[1]) - toMilliseconds(tempo[0]);

		console.log("Tempo em milli: "+temp);

		texto = replaceAll(

				"\"",
				"",
				replaceAll("\'", "", replaceAll("”", "", replaceAll("“", "",
						replaceAll("‘", "", replaceAll("’", "", texto))))));
		translateCount = 0;
		console.log('Iniciando tradução txt[' + texto + ']');
		var rybenaUrl = serverUrl
				+ 'RybenaApplication/translate/'
				+ replaceAll("\\+", "==CTSPLUS==", replaceAll('/',
						"==CTSSLASH==", base64_encode(texto)))
				+ "/" + code + "/0";

		// console.log("Texto depois do encode: ",
		// base64_decode(base64_encode(texto)), "[", base64_encode(texto),
		// "]");

		delete wordArray;
		wordArray = new Array();
		playStatus = "starting";
		count = 0;
		played = 0;
		searchCount = 0;
		buscando = false;
		emTraducao = true;
		ctsLoad(rybenaUrl, function(xhr) {
			emTraducao = false;
			var translate = xhr.responseText;
			translateArray = translate.split(";");
			if (translateArray.length > 1) {
				console.log('Terminou a requisição de tradução');
				playStatus = 'searching';
				playTxt(texto, t, callback);
			} else {
				stopRybena();
			}
		});

	}

	if (playStatus == 'paused') {
		console.log("playStatus: "+playStatus);
		playRybenaTextTimeout = setTimeout(function() {
			playTxt(texto, t, callback);
		}, 100);
		return;
	}

	if (playStatus == 'searching' || playStatus == 'playing') {
		if (searchCount < translateArray.length) {
			if (buscando === false) {
				var imageCode = translateArray[searchCount++];
				var imageDesc = translateArray[searchCount++];
				var word = getWord(imageCode);
				if (word != -1) {
					word.imageDesc = imageDesc;
					addWordArray(word);
				} else {
					if (imageCode > 0) {
						buscando = true;
						rybenaImageSearch(imageCode, imageDesc);
					}
				}
			}
			playRybenaTextTimeout = setTimeout(function() {
				playTxt(texto, t, callback);
			}, 100);
		} else {
			//console.log('Terminou de baixar as imagens para a tradução');
			playRybenaTextTimeout = setTimeout(function() {
				playTxt(texto, t, callback);
			}, 100);
		}
	}

	if ((searchCount >= 6 || searchCount >= translateArray.length)
			&& playStatus == 'searching') {
		playStatus = "playing";
		playAnimation();
	}
	// console.log('Leaving Play Rybena Text...');

}

function playAnimation() {
	if (playStatus === 'starting'
			|| playStatus === 'paused'
			|| count === 0
			|| (searchCount < translateArray.length && wordArray.length == played)) {
		setTimeout("playAnimation()", playSpeed);
		return;
	} else if (playStatus === 'playing') {

		window.document.getElementById("playBtn").style.visibility = "hidden";
		window.document.getElementById("pauseBtn").style.visibility = "visible";

		window.document.getElementById("loadingDiv").style.visibility = "hidden";

		if (wordArray.length > played) {
			var word = wordArray[played++];
			if(word.imageDesc.indexOf("[") != -1){
				//console.log("Diminuindo a velocidade ["+word.imageDesc+"]");
				playSpeed = 60;
			}else {
				//console.log("Aumentando a velocidade ["+word.imageDesc+"]");
				playSpeed = 40;
			}
			window.document.getElementById("rybenaLabel").innerHTML = word.imageDesc;
			window.document.getElementById("rybenaImage").src = 'data:image/gif;base64,'
					+ word.imageFrame;
			playTimeout = setTimeout("playAnimation()", playSpeed);
		} else {
			window.document.getElementById("rybenaLabel").innerHTML = "";
			window.document.getElementById("rybenaImage").src = webrUrl
					+ 'resource/img/rybeninha_idle.gif';
			playStatus = "stopping";
			playTimeout = setTimeout("playAnimation()", playSpeed);
		}
	} else if (playStatus === 'stopping') {
		playStatus = 'stopped';

		window.document.getElementById("playBtn").style.visibility = "visible";
		window.document.getElementById("pauseBtn").style.visibility = "hidden";

		window.document.getElementById("rybenaLabel").innerHTML = legend;
		window.document.getElementById("rybenaImage").src = webrUrl
				+ 'resource/img/rybeninha_idle.gif';
	}
}

function resumePlaying(texto, t, callback) {

	if (!(playStatus === 'paused')) {
		played = 0;
	}

	playStatus = 'playing';
	if ((translateCount < phraseArrayLibras.length)
			|| (translateCount == phraseArrayLibras.length)
			&& (searchCount <= translateArray.length)) {
		playTxt(texto, t, callback);
	}
	playAnimation();
}

function toMilliseconds(time_str) {
	// Extract hours, minutes and seconds
	var parts = time_str.split(':');
	var parts1 = parts[2].split(",");
	//console.log("part[0]:"+parts[0]+" part[1]:"+parts[1]+" part[2]:"+parts[2]+" parts1[0]:"+parts1[0]+" part1[1]:"+parts1[1]);
	parts[2] = parts1[0];
	// compute  and return total seconds
	return parts[0] * 3600000 + parts[1] * 60000 +  parts[2] * 1000 + parts1[1];  // seconds
}

function playTTS() {
	if(useTTSPagination){
		var audioElement = window.document.getElementById("tts");
		var audioElement1 = window.document.getElementById("tts1");
		if(!audioElement.paused || !audioElement1.paused){
			console.log('NAO fazer nada audio tocando');
			return;
		}
	}
	audioCheck();
	var texto = "";
	if (window.getSelection) {
		texto = "" + window.getSelection();
	} else if (document.getSelection) {
		texto = "" + document.getSelection();
	} else if (document.selection) {
		texto = "" + document.selection.createRange().text;
	}

	if (texto == null || texto == "") {
		texto = "selecionar um texto";
	}

	lastText = texto;
	if (useTTSPagination) {
		triggerTTSPaginated(texto);
	} else {
		playTtsText(texto);
	}

}

function playTtsText(texto) {
	window.document.getElementById("rybenaVozImage").src = webrUrl
			+ 'resource/img/rybeninha_voz_idle.gif';

	window.document.getElementById("playBtn").style.visibility = "hidden"; // Hide
	// Rybena
	// Libras
	// play
	// button

	texto = texto.replace("%", "porcento");
	var audioElement = window.document.getElementById("tts");

	if (navigator.appName == 'Microsoft Internet Explorer'
			|| navigator.appName == 'Netscape') {
		/*
		 * audioElement.src = serverUrl + 'TTSApplication/convertmp3/' +
		 * base64_encode(texto).replace('/', "==CTSSLASH==").replace( '+',
		 * "==CTSPLUS==") + '/' + code;
		 */

		audioElement.src = serverUrl
				+ 'TTSApplication/convertmp3/'
				+ replaceAll("\\+", "==CTSPLUS==", replaceAll('/',
						"==CTSSLASH==", base64_encode(texto))) + "/" + code
				+ "/0";

	} else {
		/*
		 * audioElement.src = serverUrl + 'TTSApplication/convert' + audioType +
		 * '/' + base64_encode(texto).replace('/', "==CTSSLASH==").replace( '+',
		 * "==CTSPLUS==") + '/' + code;
		 */

		audioElement.src = serverUrl
				+ 'TTSApplication/convert'
				+ audioType
				+ '/'
				+ replaceAll("\\+", "==CTSPLUS==", replaceAll('/',
						"==CTSSLASH==", base64_encode(texto))) + "/" + code
				+ "/0";

	}

	audioElement.type = "audio/" + audioType;
	audioElement.load();
	audioElement.play();
}

function pauseTTS() {

	var audioElement = window.document.getElementById("tts");

	if (countOpensRybenaVoz == 0) {
		resetPlayerTTS();
		audioElement.pause();
	}

	if (useTTSPagination) {

		resetPlayerTTS();
		audioElement.pause();

		var audioElement1 = window.document.getElementById("tts1");
		audioElement1.pause();
		clearTimeout(tts_pagination_timeout);

		document.getElementById('loadingDiv').style.visibility = 'hidden';
		startDownload = true;
		startDownload1 = false;
		sourceLouded = false;
		sourceLouded1 = false;
		console.log('Restarting flags on pauseTTS!!!.');
		currentPlay = 0;
		currentDownload = 0;

	} else {
		resetPlayerTTS();
		audioElement.pause();
	}
}

function restartTTS() {
	audioElement.pause();

	window.document.getElementById("rybenaVozImage").src = webrUrl
			+ 'resource/img/rybeninha_voz_idle.gif';

	texto = texto.replace("%", "porcento");
	var audioElement = window.document.getElementById("tts");
	if (internetExplorerVersion > -1) {
		audioElement.src = serverUrl + 'TTSApplication/convertmp3/' + texto
				+ '/' + code + "/0";
	} else {
		audioElement.src = serverUrl + 'TTSApplication/convert' + audioType
				+ '/' + texto + '/' + code + "/0";
	}

	audioElement.type = "audio/" + audioType;
	audioElement.load();
}

function startLibras() {

	stopRybena();
	traduzindo();
	playRybena();
}

function fecharLibras() {
	window.document.getElementById("rybenaDiv").style.visibility = "hidden";
	stopRybena();
	window.document.getElementById("playBtn").style.visibility = "hidden";
	window.document.getElementById("pauseBtn").style.visibility = "hidden";
	rybenaVisible = false;
}

function fecharTTS() {
	window.document.getElementById("rybenaVozDiv").style.visibility = "hidden";
	pauseTTS();
	ttsVisible = false;
}

function rybenaImageSearch(imageCode, imageDesc) {
	var rybenaUrl = serverUrl + 'RybenaApplication/image/' + imageCode + '/'
			+ code;
	ctsLoad(rybenaUrl, function(xhr) {
		var word = new Object();
		word.imageDesc = imageDesc;
		word.imageFrameArray = JSON.parse(xhr.responseText, null);
		setDynamicWord(imageCode, word);
		addWordArray(word);
		buscando = false;
		if (playStatus == 'waiting') {
			playStatus == 'playing';
		}
	});
}

function addWordArray(word) {

	for ( var i = 0; i < word.imageFrameArray.length; i++) {
		var frame = new Object();
		frame.imageDesc = word.imageDesc;
		frame.imageFrame = word.imageFrameArray[i];
		wordArray[count++] = frame;
	}
}

function showRybena() {

	if (rybenaVisible && !rybenaPessoalMode) {
		window.document.getElementById("rybenaDiv").style.visibility = "hidden";
		stopRybena();
		window.document.getElementById("playBtn").style.visibility = "hidden";
		window.document.getElementById("pauseBtn").style.visibility = "hidden";
		if (window.document.getElementById("loadingDiv").style.visibility == "visible") {
			window.document.getElementById("loadingDiv").style.visibility = "hidden";
		}

		rybenaVisible = false;
	} else {

		// IE>=10
		var iever = getInternetExplorerVersion();
		if (iever > -1) {
			if (iever < 10) { // If user is running //
				window.document.getElementById("rybenaDiv").style.visibility = "visible";
				window.document.getElementById("playBtn").style.visibility = "visible";
				document.getElementById("rybenaLabel").innerHTML = 'Atualize seu browser.';
				return null;
			} // Caso seja maior que 10 ok pode rodar...
		}

		window.document.getElementById("rybenaVozDiv").style.visibility = "hidden";
		ttsVisible = false;
		window.document.getElementById("rybenaDiv").style.visibility = "visible";
		window.document.getElementById("playBtn").style.visibility = "visible";
		rybenaVisible = true;

		if (countOpensRybena == 0) {
			// Primeira Vez Rybena Libras!
			console.log("Primeira Vez Rybena Libras");
			stopRybena();
			playRybenaText("selecionar um texto");
		}

		countOpensRybena++;

	}
}

function showTTS() {
	resetPlayerTTS();
	if (ttsVisible && !rybenaPessoalMode) {
		window.document.getElementById("rybenaVozDiv").style.visibility = "hidden";
		ttsVisible = false;
	} else {

		// IE>=10
		var iever = getInternetExplorerVersion();
		if (iever > -1) {
			if (iever < 10) { // If user is running //
				window.document.getElementById("rybenaDiv").style.visibility = "visible";
				document.getElementById("rybenaLabel").innerHTML = 'Atualize seu browser.';
				return null;
			} // Caso seja maior que 10 ok pode rodar...
		}

		// adicionado para resolver o problema do botao pause e play nao ficarem
		// hidden
		stopRybena();

		window.document.getElementById("rybenaDiv").style.visibility = "hidden";
		window.document.getElementById("playBtn").style.visibility = "hidden";
		window.document.getElementById("pauseBtn").style.visibility = "hidden";

		rybenaVisible = false;
		window.document.getElementById("rybenaVozDiv").style.visibility = "visible";
		ttsVisible = true;
		requestTextSelecion();

		// countOpensRybenaVoz++;

		// if(countOpensRybenaVoz == 1)
		// {
		// Primeira Vez Rybena Voz!
		// PlayRybenaText("favor selecionar um texto.");
		// }

	}
}

function playCTS() {
	if (playStatus === 'starting'
			|| playStatus === 'paused'
			|| count === 0
			|| (searchCount < translateArray.length && wordArray.length == played)) {
		setTimeout("playCTS()", playSpeed);
		return;
	} else if (playStatus === 'playing') {

		window.document.getElementById("playBtn").style.visibility = "hidden";
		window.document.getElementById("pauseBtn").style.visibility = "visible";

		window.document.getElementById("loadingDiv").style.visibility = "hidden";

		if (wordArray.length > played) {
			var word = wordArray[played++];
			window.document.getElementById("rybenaLabel").innerHTML = word.imageDesc;
			window.document.getElementById("rybenaImage").src = 'data:image/gif;base64,'
					+ word.imageFrame;
			playTimeout = setTimeout("playCTS()", playSpeed);
		} else {
			window.document.getElementById("rybenaLabel").innerHTML = "";
			window.document.getElementById("rybenaImage").src = webrUrl
					+ 'resource/img/rybeninha_idle.gif';
			playStatus = "stopping";
			playTimeout = setTimeout("playCTS()", playSpeed);
		}
	} else if (playStatus === 'stopping') {
		playStatus = 'stopped';

		window.document.getElementById("playBtn").style.visibility = "visible";
		window.document.getElementById("pauseBtn").style.visibility = "hidden";

		window.document.getElementById("rybenaLabel").innerHTML = legend;
		window.document.getElementById("rybenaImage").src = webrUrl
				+ 'resource/img/rybeninha_idle.gif';
	}
}

function repeatRybena() {

	if (!(playStatus === 'paused')) {
		played = 0;
	}

	playStatus = 'playing';
	if ((translateCount < phraseArrayLibras.length)
			|| (translateCount == phraseArrayLibras.length)
			&& (searchCount <= translateArray.length)) {
		playRybenaText(text);
	}
	playCTS();
}

function stopRybena() {
	window.document.getElementById("rybenaLabel").innerHTML = legend;
	window.document.getElementById("rybenaImage").src = webrUrl
			+ 'resource/img/rybeninha_idle.gif';
	window.document.getElementById("playBtn").style.visibility = "visible";
	window.document.getElementById("pauseBtn").style.visibility = "hidden";
	if (playStatus == 'playing' || playStatus == 'searching'
			|| playStatus == 'paused') {
		// playStatusStopRequest = true;
		clearTimeout(playRybenaTextTimeout);
		clearTimeout(playTimeout);
		played = 0;
		playStatus = 'stopped';
		buscando = false;
		emTraducao = false;
		translateCount = 0;
	}
}

function pauseRybena() {

	window.document.getElementById("playBtn").style.visibility = "visible";
	window.document.getElementById("pauseBtn").style.visibility = "hidden";
	clearTimeout(playRybenaTextTimeout);
	clearTimeout(playTimeout);
	playStatus = 'paused';
}

function speedUpRybena() {
	playSpeed = playSpeed - 10;
}

function speedDownRybena() {
	playSpeed = playSpeed + 10;
}

function sleep(milliSeconds) {
	var startTime = new Date().getTime();
	while (new Date().getTime() < startTime + milliSeconds)
		;
}

function onloadTTS() {

	window.document.getElementById("rybenaVozLabel").innerHTML = "Carregando...";
	window.document.getElementById("rybenaVozImage").src = webrUrl
			+ 'resource/img/rybeninha_voz_idle.gif';

	if (document.getElementById("loadingDiv") == null) {
		var loadingDiv = document.createElement("div");
		loadingDiv.id = "loadingDiv";
		loadingDiv.style.cssText = "width: 120px; height: 120px; position: absolute; top: 0px; left: 120px; z-index:9999;";
	} else {
		var loadingDiv = document.getElementById("loadingDiv");
		window.document.getElementById("loadingDiv").style.visibility = "visible";

	}

	loadingDiv.innerHTML = "<img width=120 height=120 src='" + webrUrl
			+ "resource/img/loading.gif'></img>";
	window.document.getElementById("rybenaVozDiv").appendChild(loadingDiv);

}

function onplayTTS() {
	window.document.getElementById("rybenaVozLabel").innerHTML = "";
	window.document.getElementById("rybenaVozImage").src = webrUrl
			+ 'resource/img/rybeninha_voz.gif';
	window.document.getElementById("loadingDiv").style.visibility = "hidden";

}

function traduzindo() {
	window.document.getElementById("rybenaLabel").innerHTML = "Carregando...";
	window.document.getElementById("rybenaImage").src = webrUrl
			+ 'resource/img/rybeninha_idle.gif';

	if (document.getElementById("loadingDiv") == null) {
		var loadingDiv = document.createElement("div");
		loadingDiv.id = "loadingDiv";
		loadingDiv.style.cssText = "width: 120px; height: 120px; position: absolute; top: 200px; left: 120px; z-index:9999;";
	} else {
		var loadingDiv = document.getElementById("loadingDiv");
		window.document.getElementById("loadingDiv").style.visibility = "visible";

	}

	loadingDiv.innerHTML = "<img width=120 height=120 src='" + webrUrl
			+ "resource/img/loading.gif'></img>";
	window.document.getElementById("rybenaDiv").appendChild(loadingDiv);

}

function load() {

	// Adicionado para resolver o problema da rybenaDiv quando clicada o evento
	// de mouseUp fazia com que a trdução fosse repetida
	document.getElementById("rybenaDiv").onmousedown = function() {
		clearSelection();
	};

	document.getElementById("rybenaVozDiv").onmousedown = function() {
		clearSelection();
	};

	window.document.getElementById("rybenaLabel").innerHTML = legend;
	window.document.getElementById("rybenaImage").src = webrUrl
			+ 'resource/img/rybeninha_idle.gif';
	window.document.getElementById("loadingDiv").style.visibility = "hidden";

	resetPlayerTTS();

	if (window.captureEvents) {

		document.captureEvents(Event.MOUSEUP);
		document.captureEvents(Event.KEYDOWN);
		document.captureEvents(Event.MOUSEDOWN);
		document.onmouseup = getSelectedText;
		document.onmousedown = clearSelection;
		document.onkeydown = getSelectedText;

	} else {

		document.onmouseup = getSelectedText;
		document.onkeydown = getSelectedText;
		document.onmousedown = clearSelection;
	}

}

function resetPlayerTTS() {
	window.document.getElementById("rybenaVozLabel").innerHTML = "";
	window.document.getElementById("rybenaVozImage").src = webrUrl
			+ 'resource/img/rybeninha_voz_idle.gif';
	var audioElement = window.document.getElementById("tts");
	audioElement.pause();
}

function requestTextSelecion() {
	audioCheck();
	var audioElement = window.document.getElementById("tts");
	// Modificação IE!

	if (internetExplorerVersion > -1) { // If user is running
		// any IE!
		// console.log('eh Internet explorer');
		audioElement.setAttribute("src", webrUrl
				+ "resource/sound/selecionar.mp3");
		audioElement.type = "audio/mpeg3";
		audioElement.play();
		audioElement.addEventListener('ended',
				rybenaVoiceFirstPlaySelectedTextEventListener);
		return null;
	} else {
		// console.log('nao eh Internet explorer');
		audioElement.setAttribute("src", webrUrl + "resource/sound/selecionar."
				+ audioType);
		audioElement.type = "audio/" + audioType;
		audioElement.play();

		audioElement.addEventListener('ended',
				rybenaVoiceFirstPlaySelectedTextEventListener);
	}

}

function rybenaVoiceFirstPlaySelectedTextEventListener() {

	var audioElement = document.getElementById("tts");

	resetPlayerTTS();
	audioElement.pause();

	audioElement.removeEventListener('ended',
			rybenaVoiceFirstPlaySelectedTextEventListener);

}

function audioCheck() {
	var audio = new Audio();
	var types = new Array(3);
	var i = 0;

	types[0] = 'ogg';
	types[1] = 'mp3';
	types[2] = 'wav';

	for (i = 0; i < 3; i++) {
		if (audio.canPlayType("audio/" + types[i]) != "") {
			audioType = "wav";
			break;
		}
	}
}

// Funcao responsavel pela paginacao do texto
function triggerTTSPaginated(text) {
	// countOpensRybenaVoz = 1000;
	// var texto_completo = document.getElementById('texto').value;
	if (internetExplorerVersion > -1) {
		phraseArray = cleanArray(replaceAll(

				"\"",
				"",
				replaceAll("\'", "", replaceAll("”", "", replaceAll("“", "",
						replaceAll("‘", "", replaceAll("’", "", text))))))
				.split(/\n|\. /));
	} else {
		phraseArray = cleanArray(replaceAll(

				"\"",
				"",
				replaceAll("\'", "", replaceAll("”", "", replaceAll("“", "",
						replaceAll("‘", "", replaceAll("’", "", text))))))
				.split(/\n|\. |, /));
	}
	playTTSpaginated();
}

// Funcao reentrante de reproducao
function playTTSpaginated() {
	var audioElement = window.document.getElementById("tts");
	var audioElement1 = window.document.getElementById("tts1");

	if (currentPlay == phraseArray.length && audioElement.paused
			&& audioElement1.paused) {
		clearTimeout(tts_pagination_timeout);
		document.getElementById('loadingDiv').style.visibility = 'hidden';
		startDownload = true;
		startDownload1 = false;
		sourceLouded = false;
		sourceLouded1 = false;
		console.log('Restarting flags!!!.');
		currentPlay = 0;
		currentDownload = 0;

		resetPlayerTTS();

		return;
	}

	if (phraseArray.length > 0) {
		// Tem arquivos para serem tocados.
		if (startDownload && sourceLouded == false) {
			console.log('Fazendo Download audioElement');
			if (internetExplorerVersion > -1) {
				// console.log("Esta utilizando IE");
				audioElement.src = serverUrl
						+ 'TTSApplication/convertmp3/'
						+ replaceAll("\\+", "==CTSPLUS==", replaceAll('/',
								"==CTSSLASH==",
								base64_encode(phraseArray[currentDownload])))
						+ '/' + code + "/" + currentDownload;
				audioElement.type = "audio/mpeg3";
			} else {
				// console.log("NAO esta utilizando IE");
				audioElement.src = serverUrl
						+ 'TTSApplication/convertwav/'
						+ replaceAll("\\+", "==CTSPLUS==", replaceAll('/',
								"==CTSSLASH==",
								base64_encode(phraseArray[currentDownload])))
						+ '/' + code + "/" + currentDownload;
				audioElement.type = "audio/wav";
			}
			audioElement.load();
			currentDownload = currentDownload + 1;
			sourceLouded = true;
		}
		if (startDownload1 && sourceLouded1 == false) {
			console.log('Fazendo Download audioElement1');
			if (internetExplorerVersion > -1) {
				// console.log("Esta utilizando IE1");
				audioElement1.src = serverUrl
						+ 'TTSApplication/convertmp3/'
						+ replaceAll("\\+", "==CTSPLUS==", replaceAll('/',
								"==CTSSLASH==",
								base64_encode(phraseArray[currentDownload])))
						+ '/' + code + "/" + currentDownload;
				audioElement.type = "audio/mpeg3";

			} else {
				// console.log("NAO esta utilizando IE1");
				audioElement1.src = serverUrl
						+ 'TTSApplication/convertwav/'
						+ replaceAll("\\+", "==CTSPLUS==", replaceAll('/',
								"==CTSSLASH==",
								base64_encode(phraseArray[currentDownload])))
						+ '/' + code + "/" + currentDownload;
				audioElement.type = "audio/wav";
			}
			audioElement1.load();
			currentDownload = currentDownload + 1;
			sourceLouded1 = true;
		}

		if (currentPlay == 0) {
			if (audioElement.paused && audioElement1.paused) {
				console.log('Tocando audioElement');
				if (audioElement.src) {
					// console.log("Primera frase audioElement.src carregado");
					audioElement.play();
					currentPlay = currentPlay + 1;
				}
			}
		}

		if (audioElement.paused && sourceLouded == false
				&& currentDownload < phraseArray.length) {
			console.log('Duracao [' + audioElement.duration + '] Current ['
					+ audioElement.currentTime + ']');
			startDownload = true;
			startDownload1 = false;
		}

		if (audioElement1.paused && sourceLouded1 == false
				&& currentDownload < phraseArray.length) {
			console.log('Duracao1 [' + audioElement1.duration + '] Current1 ['
					+ audioElement1.currentTime + ']');
			if(internetExplorerVersion == -1){
				startDownload = false;
				startDownload1 = true;
			} else if(phraseArray.length != 2){
				console.log('phraseArray.length nao eh dois');
				startDownload = false;
				startDownload1 = true;
			} else if(audioElement.currentTime > (audioElement.duration / 2)){
				console.log('Setando flags para inicia download audioElement1');
				startDownload = false;
				startDownload1 = true;
			} else {
				console.log('Aguardando... [' + audioElement.duration + '] Current ['
						+ audioElement.currentTime + ']');
			}
		}

		audioElement.addEventListener('ended', function() {
			// console.log('audioElement ended');
			if(internetExplorerVersion > -1){
				audioElement.pause();
			}
			if (audioElement1.paused && audioElement.paused) {
				if (audioElement1.src) {
					if (currentPlay < phraseArray.length) {
						console.log('addEventListener Tocando audioElement1');
						audioElement1.play();
						// console.log('Increment currentPlay');
						currentPlay = currentPlay + 1;
						sourceLouded = false;
					}
				}
			}
		}, false);

		audioElement1.addEventListener('ended', function() {
			// console.log('audioElement1 ended');
			if(internetExplorerVersion > -1){
				audioElement1.pause();
			}
			if (audioElement1.paused && audioElement.paused) {
				if (audioElement.src) {
					if (currentPlay < phraseArray.length) {
						console.log('addEventListener Tocando audioElement');
						audioElement.play();
						// console.log('Increment currentPlay');
						currentPlay = currentPlay + 1;
						sourceLouded1 = false;
					}
				}
			}
		}, false);

		if (internetExplorerVersion > -1) {
			// console.log('timeout 600');
			tts_pagination_timeout = setTimeout(function() {
				playTTSpaginated();
			}, 600);
		} else {
			// console.log('timeout 100');
			tts_pagination_timeout = setTimeout(function() {
				playTTSpaginated();
			}, 100);
		}
	}
}

function httpGet(theUrl) {
	var xmlHttp = null;
	xmlHttp = new XMLHttpRequest();
	xmlHttp.open("GET", theUrl, false);
	xmlHttp.send(null);
	return xmlHttp.response;
}

function cleanArray(actual) {

	String.prototype.trim = function() {
		return this.replace(/^\s+|\s+$/g, "");
	};

	var newArray = new Array();
	for ( var i = 0; i < actual.length; i++) {

		actual[i] = actual[i].trim();

		if (actual[i]) {
			newArray.push(actual[i]);
		}
	}
	return newArray;
}

function hookEvent(element, eventName, callback) {
	if (typeof (element) == "string")
		element = document.getElementById(element);
	if (element == null)
		return;
	if (element.addEventListener) {
		element.addEventListener(eventName, callback, false);
	} else if (element.attachEvent)
		element.attachEvent("on" + eventName, callback);
}

function unhookEvent(element, eventName, callback) {
	if (typeof (element) == "string")
		element = document.getElementById(element);
	if (element == null)
		return;
	if (element.removeEventListener)
		element.removeEventListener(eventName, callback, false);
	else if (element.detachEvent)
		element.detachEvent("on" + eventName, callback);
}

function cancelEvent(e) {
	e = e ? e : window.event;
	if (e.stopPropagation)
		e.stopPropagation();
	if (e.preventDefault)
		e.preventDefault();
	e.cancelBubble = true;
	e.cancel = true;
	e.returnValue = false;
	return false;
}

function Position(x, y) {
	this.X = x;
	this.Y = y;

	this.Add = function(val) {
		var newPos = new Position(this.X, this.Y);
		if (val != null) {
			if (!isNaN(val.X))
				newPos.X += val.X;
			if (!isNaN(val.Y))
				newPos.Y += val.Y;
		}
		return newPos;
	};

	this.Subtract = function(val) {
		var newPos = new Position(this.X, this.Y);
		if (val != null) {
			if (!isNaN(val.X))
				newPos.X -= val.X;
			if (!isNaN(val.Y))
				newPos.Y -= val.Y;
		}
		return newPos;
	};

	this.Min = function(val) {
		var newPos = new Position(this.X, this.Y);
		if (val == null)
			return newPos;

		if (!isNaN(val.X) && this.X > val.X)
			newPos.X = val.X;
		if (!isNaN(val.Y) && this.Y > val.Y)
			newPos.Y = val.Y;

		return newPos;
	};

	this.Max = function(val) {
		var newPos = new Position(this.X, this.Y);
		if (val == null)
			return newPos;

		if (!isNaN(val.X) && this.X < val.X)
			newPos.X = val.X;
		if (!isNaN(val.Y) && this.Y < val.Y)
			newPos.Y = val.Y;

		return newPos;
	};

	this.Bound = function(lower, upper) {
		var newPos = this.Max(lower);
		return newPos.Min(upper);
	};

	this.Check = function() {
		var newPos = new Position(this.X, this.Y);
		if (isNaN(newPos.X))
			newPos.X = 0;
		if (isNaN(newPos.Y))
			newPos.Y = 0;
		return newPos;
	};

	this.Apply = function(element) {
		if (typeof (element) == "string")
			element = document.getElementById(element);
		if (element == null)
			return;
		if (!isNaN(this.X))
			element.style.left = this.X + 'px';
		if (!isNaN(this.Y))
			element.style.top = this.Y + 'px';
	};
}

function absoluteCursorPostion(eventObj) {
	eventObj = eventObj ? eventObj : window.event;

	if (isNaN(window.scrollX))
		return new Position(eventObj.clientX
				+ document.documentElement.scrollLeft
				+ document.body.scrollLeft, eventObj.clientY
				+ document.documentElement.scrollTop + document.body.scrollTop);
	else
		return new Position(eventObj.clientX + window.scrollX, eventObj.clientY
				+ window.scrollY);
}

function dragObject(element, attachElement, lowerBound, upperBound,
		startCallback, moveCallback, endCallback, attachLater) {
	if (typeof (element) == "string")
		element = document.getElementById(element);
	if (element == null)
		return;

	if (lowerBound != null && upperBound != null) {
		var temp = lowerBound.Min(upperBound);
		upperBound = lowerBound.Max(upperBound);
		lowerBound = temp;
	}

	var cursorStartPos = null;
	var elementStartPos = null;
	var dragging = false;
	var listening = false;
	var disposed = false;

	function dragStart(eventObj) {
		if (dragging || !listening || disposed)
			return;
		dragging = true;

		if (startCallback != null)
			startCallback(eventObj, element);

		cursorStartPos = absoluteCursorPostion(eventObj);

		elementStartPos = new Position(parseInt(element.style.left),
				parseInt(element.style.top));

		elementStartPos = elementStartPos.Check();

		hookEvent(document, "mousemove", dragGo);
		hookEvent(document, "mouseup", dragStopHook);

		return cancelEvent(eventObj);
	}

	function dragGo(eventObj) {
		if (!dragging || disposed)
			return;

		var newPos = absoluteCursorPostion(eventObj);
		newPos = newPos.Add(elementStartPos).Subtract(cursorStartPos);
		newPos = newPos.Bound(lowerBound, upperBound);
		newPos.Apply(element);
		if (moveCallback != null)
			moveCallback(newPos, element);

		return cancelEvent(eventObj);
	}

	function dragStopHook(eventObj) {
		dragStop();
		return cancelEvent(eventObj);
	}

	function dragStop() {
		if (!dragging || disposed)
			return;
		unhookEvent(document, "mousemove", dragGo);
		unhookEvent(document, "mouseup", dragStopHook);
		cursorStartPos = null;
		elementStartPos = null;
		if (endCallback != null)
			endCallback(element);
		dragging = false;
	}

	this.Dispose = function() {
		if (disposed)
			return;
		this.StopListening(true);
		element = null;
		attachElement = null;
		lowerBound = null;
		upperBound = null;
		startCallback = null;
		moveCallback = null;
		endCallback = null;
		disposed = true;
	};

	this.StartListening = function() {
		if (listening || disposed)
			return;
		listening = true;
		hookEvent(attachElement, "mousedown", dragStart);
	};

	this.StopListening = function(stopCurrentDragging) {
		if (!listening || disposed)
			return;
		unhookEvent(attachElement, "mousedown", dragStart);
		listening = false;

		if (stopCurrentDragging && dragging)
			dragStop();
	};

	this.IsDragging = function() {
		return dragging;
	};

	this.IsListening = function() {
		return listening;
	};

	this.IsDisposed = function() {
		return disposed;
	};

	if (typeof (attachElement) == "string")
		attachElement = document.getElementById(attachElement);
	if (attachElement == null)
		attachElement = element;

	if (!attachLater)
		this.StartListening();
}

var exampB = null;
var exampBV = null;

function initialize()
{
	exampB = new dragObject("rybenaDiv", "rybenaHandle");
	exampBV = new dragObject("rybenaVozDiv", "rybenaHandle");
}

function detectPlugin( name ) {
   name = name.toLowerCase();

   for( var i = 0; navigator.plugins[ i ]; ++i ) {
      if( navigator.plugins[ i ].name.toLowerCase().indexOf( name ) > -1 )
    	  alert(navigator.plugins[ i ].name.toLowerCase());
         return true;
   }
   alert("nao achou nada");
   return false;
}
 function base64_encode(data) {

	var b64 = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
	var o1, o2, o3, h1, h2, h3, h4, bits, i = 0, ac = 0, enc = "", tmp_arr = [];

	if (!data) {
		return data;
	}

	data = this.utf8_encode(data);

	do {
		o1 = data.charCodeAt(i++);
		o2 = data.charCodeAt(i++);
		o3 = data.charCodeAt(i++);

		bits = o1 << 16 | o2 << 8 | o3;

		h1 = bits >> 18 & 0x3f;
		h2 = bits >> 12 & 0x3f;
		h3 = bits >> 6 & 0x3f;
		h4 = bits & 0x3f;

		tmp_arr[ac++] = b64.charAt(h1) + b64.charAt(h2) + b64.charAt(h3)
				+ b64.charAt(h4);
	} while (i < data.length);

	enc = tmp_arr.join('');

	var r = data.length % 3;

	return ((r ? enc.slice(0, r - 3) : enc) + '==='.slice(r || 3));
}

function base64_decode(data) {

	var b64 = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";

	var o1, o2, o3, h1, h2, h3, h4, bits, i = 0, ac = 0, dec = "", tmp_arr = [];

	if (!data) {
		return data;
	}

	do {
		h1 = b64.indexOf(data.charAt(i++));
		h2 = b64.indexOf(data.charAt(i++));
		h3 = b64.indexOf(data.charAt(i++));
		h4 = b64.indexOf(data.charAt(i++));

		bits = h1 << 18 | h2 << 12 | h3 << 6 | h4;

		o1 = bits >> 16 & 0xff;
		o2 = bits >> 8 & 0xff;
		o3 = bits & 0xff;

		if (h3 == 64) {
			tmp_arr[ac++] = String.fromCharCode(o1);
		} else if (h4 == 64) {
			tmp_arr[ac++] = String.fromCharCode(o1, o2);
		} else {
			tmp_arr[ac++] = String.fromCharCode(o1, o2, o3);
		}
	} while (i < data.length);

	dec = tmp_arr.join('');

	return dec;
}

function char_convert(str) {

    var chars = ["©","Û","®","ž","Ü","Ÿ","Ý","$","Þ","%","¡","ß","¢","à","£","á","À","¤","â","Á","¥","ã","Â","¦","ä","Ã","§","å","Ä","¨","æ","Å","©","ç","Æ","ª","è","Ç","«","é","È","¬","ê","É","­","ë","Ê","®","ì","Ë","¯","í","Ì","°","î","Í","±","ï","Î","²","ð","Ï","³","ñ","Ð","´","ò","Ñ","µ","ó","Õ","¶","ô","Ö","·","õ","Ø","¸","ö","Ù","¹","÷","Ú","º","ø","Û","»","ù","Ü","@","¼","ú","Ý","½","û","Þ","€","¾","ü","ß","¿","ý","à","‚","À","þ","á","ƒ","Á","ÿ","å","„","Â","æ","…","Ã","ç","†","Ä","è","‡","Å","é","ˆ","Æ","ê","‰","Ç","ë","Š","È","ì","‹","É","í","Œ","Ê","î","Ë","ï","Ž","Ì","ð","Í","ñ","Î","ò","‘","Ï","ó","’","Ð","ô","“","Ñ","õ","”","Ò","ö","•","Ó","ø","–","Ô","ù","—","Õ","ú","˜","Ö","û","™","×","ý","š","Ø","þ","›","Ù","ÿ","œ","Ú"];
    var codes = ["&copy;","&#219;","&reg;","&#158;","&#220;","&#159;","&#221;","&#36;","&#222;","&#37;","&#161;","&#223;","&#162;","&#224;","&#163;","&#225;","&Agrave;","&#164;","&#226;","&Aacute;","&#165;","&#227;","&Acirc;","&#166;","&#228;","&Atilde;","&#167;","&#229;","&Auml;","&#168;","&#230;","&Aring;","&#169;","&#231;","&AElig;","&#170;","&#232;","&Ccedil;","&#171;","&#233;","&Egrave;","&#172;","&#234;","&Eacute;","&#173;","&#235;","&Ecirc;","&#174;","&#236;","&Euml;","&#175;","&#237;","&Igrave;","&#176;","&#238;","&Iacute;","&#177;","&#239;","&Icirc;","&#178;","&#240;","&Iuml;","&#179;","&#241;","&ETH;","&#180;","&#242;","&Ntilde;","&#181;","&#243;","&Otilde;","&#182;","&#244;","&Ouml;","&#183;","&#245;","&Oslash;","&#184;","&#246;","&Ugrave;","&#185;","&#247;","&Uacute;","&#186;","&#248;","&Ucirc;","&#187;","&#249;","&Uuml;","&#64;","&#188;","&#250;","&Yacute;","&#189;","&#251;","&THORN;","&#128;","&#190;","&#252","&szlig;","&#191;","&#253;","&agrave;","&#130;","&#192;","&#254;","&aacute;","&#131;","&#193;","&#255;","&aring;","&#132;","&#194;","&aelig;","&#133;","&#195;","&ccedil;","&#134;","&#196;","&egrave;","&#135;","&#197;","&eacute;","&#136;","&#198;","&ecirc;","&#137;","&#199;","&euml;","&#138;","&#200;","&igrave;","&#139;","&#201;","&iacute;","&#140;","&#202;","&icirc;","&#203;","&iuml;","&#142;","&#204;","&eth;","&#205;","&ntilde;","&#206;","&ograve;","&#145;","&#207;","&oacute;","&#146;","&#208;","&ocirc;","&#147;","&#209;","&otilde;","&#148;","&#210;","&ouml;","&#149;","&#211;","&oslash;","&#150;","&#212;","&ugrave;","&#151;","&#213;","&uacute;","&#152;","&#214;","&ucirc;","&#153;","&#215;","&yacute;","&#154;","&#216;","&thorn;","&#155;","&#217;","&yuml;","&#156;","&#218;"];

    var x,i;

    for(x=0; x<chars.length; x++){
        for (i=0; i<str.length; i++){
            str[i] = str.charAt(i).replace(chars[x], codes[x]);
        }
    }
 }


function utf8_decode (str_data) {

	  var tmp_arr = [],
	    i = 0,
	    ac = 0,
	    c1 = 0,
	    c2 = 0,
	    c3 = 0;

	  str_data += '';

	  while (i < str_data.length) {
	    c1 = str_data.charCodeAt(i);
	    if (c1 < 128) {
	      tmp_arr[ac++] = String.fromCharCode(c1);
	      i++;
	    } else if (c1 > 191 && c1 < 224) {
	      c2 = str_data.charCodeAt(i + 1);
	      tmp_arr[ac++] = String.fromCharCode(((c1 & 31) << 6) | (c2 & 63));
	      i += 2;
	    } else {
	      c2 = str_data.charCodeAt(i + 1);
	      c3 = str_data.charCodeAt(i + 2);
	      tmp_arr[ac++] = String.fromCharCode(((c1 & 15) << 12) | ((c2 & 63) << 6) | (c3 & 63));
	      i += 3;
	    }
	  }

	  return tmp_arr.join('');
	}

function utf8_encode (argString) {


	  if (argString === null || typeof argString === "undefined") {
	    return "";
	  }

	  var string = (argString + '');
	  var utftext = '',
	    start, end, stringl = 0;

	  start = end = 0;
	  stringl = string.length;
	  for (var n = 0; n < stringl; n++) {
	    var c1 = string.charCodeAt(n);
	    var enc = null;

	    if (c1 < 128) {
	      end++;
	    } else if (c1 > 127 && c1 < 2048) {
	      enc = String.fromCharCode((c1 >> 6) | 192, (c1 & 63) | 128);
	    } else {
	      enc = String.fromCharCode((c1 >> 12) | 224, ((c1 >> 6) & 63) | 128, (c1 & 63) | 128);
	    }
	    if (enc !== null) {
	      if (end > start) {
	        utftext += string.slice(start, end);
	      }
	      utftext += enc;
	      start = end = n + 1;
	    }
	  }

	  if (end > start) {
	    utftext += string.slice(start, stringl);
	  }

	  return utftext;
	}
 var JSON;
if (!JSON) {
    JSON = {};
}

(function () {
    'use strict';

    function f(n) {
        return n < 10 ? '0' + n : n;
    }

    if (typeof Date.prototype.toJSON !== 'function') {

        Date.prototype.toJSON = function (key) {

            return isFinite(this.valueOf())
                ? this.getUTCFullYear()     + '-' +
                    f(this.getUTCMonth() + 1) + '-' +
                    f(this.getUTCDate())      + 'T' +
                    f(this.getUTCHours())     + ':' +
                    f(this.getUTCMinutes())   + ':' +
                    f(this.getUTCSeconds())   + 'Z'
                : null;
        };

        String.prototype.toJSON      =
            Number.prototype.toJSON  =
            Boolean.prototype.toJSON = function (key) {
                return this.valueOf();
            };
    }

    var cx = /[\u0000\u00ad\u0600-\u0604\u070f\u17b4\u17b5\u200c-\u200f\u2028-\u202f\u2060-\u206f\ufeff\ufff0-\uffff]/g,
        escapable = /[\\\"\x00-\x1f\x7f-\x9f\u00ad\u0600-\u0604\u070f\u17b4\u17b5\u200c-\u200f\u2028-\u202f\u2060-\u206f\ufeff\ufff0-\uffff]/g,
        gap,
        indent,
        meta = {
            '\b': '\\b',
            '\t': '\\t',
            '\n': '\\n',
            '\f': '\\f',
            '\r': '\\r',
            '"' : '\\"',
            '\\': '\\\\'
        },
        rep;


    function quote(string) {

        escapable.lastIndex = 0;
        return escapable.test(string) ? '"' + string.replace(escapable, function (a) {
            var c = meta[a];
            return typeof c === 'string'
                ? c
                : '\\u' + ('0000' + a.charCodeAt(0).toString(16)).slice(-4);
        }) + '"' : '"' + string + '"';
    }


    function str(key, holder) {

        var i,
            k,
            v,
            length,
            mind = gap,
            partial,
            value = holder[key];


        if (value && typeof value === 'object' &&
                typeof value.toJSON === 'function') {
            value = value.toJSON(key);
        }


        if (typeof rep === 'function') {
            value = rep.call(holder, key, value);
        }


        switch (typeof value) {
        case 'string':
            return quote(value);

        case 'number':

            return isFinite(value) ? String(value) : 'null';

        case 'boolean':
        case 'null':

            return String(value);

        case 'object':


            if (!value) {
                return 'null';
            }


            gap += indent;
            partial = [];


            if (Object.prototype.toString.apply(value) === '[object Array]') {


                length = value.length;
                for (i = 0; i < length; i += 1) {
                    partial[i] = str(i, value) || 'null';
                }

                v = partial.length === 0
                    ? '[]'
                    : gap
                    ? '[\n' + gap + partial.join(',\n' + gap) + '\n' + mind + ']'
                    : '[' + partial.join(',') + ']';
                gap = mind;
                return v;
            }


            if (rep && typeof rep === 'object') {
                length = rep.length;
                for (i = 0; i < length; i += 1) {
                    if (typeof rep[i] === 'string') {
                        k = rep[i];
                        v = str(k, value);
                        if (v) {
                            partial.push(quote(k) + (gap ? ': ' : ':') + v);
                        }
                    }
                }
            } else {


                for (k in value) {
                    if (Object.prototype.hasOwnProperty.call(value, k)) {
                        v = str(k, value);
                        if (v) {
                            partial.push(quote(k) + (gap ? ': ' : ':') + v);
                        }
                    }
                }
            }


            v = partial.length === 0
                ? '{}'
                : gap
                ? '{\n' + gap + partial.join(',\n' + gap) + '\n' + mind + '}'
                : '{' + partial.join(',') + '}';
            gap = mind;
            return v;
        }
    }


    if (typeof JSON.stringify !== 'function') {
        JSON.stringify = function (value, replacer, space) {


            var i;
            gap = '';
            indent = '';

            if (typeof space === 'number') {
                for (i = 0; i < space; i += 1) {
                    indent += ' ';
                }


            } else if (typeof space === 'string') {
                indent = space;
            }


            rep = replacer;
            if (replacer && typeof replacer !== 'function' &&
                    (typeof replacer !== 'object' ||
                    typeof replacer.length !== 'number')) {
                throw new Error('JSON.stringify');
            }


            return str('', {'': value});
        };
    }



    if (typeof JSON.parse !== 'function') {
        JSON.parse = function (text, reviver) {


            var j;

            function walk(holder, key) {



                var k, v, value = holder[key];
                if (value && typeof value === 'object') {
                    for (k in value) {
                        if (Object.prototype.hasOwnProperty.call(value, k)) {
                            v = walk(value, k);
                            if (v !== undefined) {
                                value[k] = v;
                            } else {
                                delete value[k];
                            }
                        }
                    }
                }
                return reviver.call(holder, key, value);
            }



            text = String(text);
            cx.lastIndex = 0;
            if (cx.test(text)) {
                text = text.replace(cx, function (a) {
                    return '\\u' +
                        ('0000' + a.charCodeAt(0).toString(16)).slice(-4);
                });
            }


            if (/^[\],:{}\s]*$/
                    .test(text.replace(/\\(?:["\\\/bfnrt]|u[0-9a-fA-F]{4})/g, '@')
                        .replace(/"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g, ']')
                        .replace(/(?:^|:|,)(?:\s*\[)+/g, ''))) {


                j = eval('(' + text + ')');


                return typeof reviver === 'function'
                    ? walk({'': j}, '')
                    : j;
            }

            throw new SyntaxError('JSON.parse');
        };
    }
}());
 var rybenaRepositoryUrl = webrUrl; // Isso pode porque a variavel foi definida
// em rybena.js e ambos sao concatenados
// antes de entregar o codigo para o
// usuario.

function includeRybenaAsync() {
	var barraStr;
	//if (getInternetExplorerVersion() == -1) {

		barraStr = "<div id=\"headerRybena\">"
						+ "<div class=\"rybena\">"
						+ "<img width=\"135px\" height=\"37px\" usemap=\"#rybenamapTopo\" alt=\"Rybena;\" src=\""
						+ rybenaRepositoryUrl
						+ "images/barraRybena.png\" border=\"0\">"
						+ "<map id=\"rybenamapTopo\" name=\"rybenamapTopo\">"
						+ "<area style=\"cursor: pointer\" onclick=\"showRybena()\" title=\"Clique aqui para traduzir para LIBRAS o texto selecionado.\" alt=\"Clique aqui para traduzir para LIBRAS o texto selecionado.\" coords=\"70,0,99,29\" shape=\"rect\">"
						+ "<area style=\"cursor: pointer; background-color: Green;\" onclick=\"showTTS()\" title=\"Clique aqui para ouvir o texto selecionado.\" alt=\"Clique aqui para ouvir o texto selecionado.\" coords=\"105,1,138,32\" shape=\"rect\">"
						+ "</map>"
						+ "<div id=\"rybenaDiv\" style=\"background-image: url('"
						+ rybenaRepositoryUrl
						+ "resource/img/borda_libras.png'); height: 358px; width: 211px; position: fixed; top: 60px; left: 570px; align: center; visibility: hidden; z-index: 9999;\">"
						+ "<div onclick=\"fecharLibras();\" title=\"Fechar\" style=\"height: 24px; width: 22px; position: relative; top: 3px; left: 182px;\"></div>"
						+ "<div style=\"position: absolute; top: 37px; left: 9px; margin-left: auto; margin-right: auto;\">"
						+ "<img id=\"rybenaImage\" alt=\"Rybená\" src=\""
						+ rybenaRepositoryUrl
						+ "resource/img/rybeninha_idle.gif\" width=\"194\" border=\"0\">"
						+ "</div>"
						+ "<div style=\"position: absolute; top: 297px; width: 211px; text-align: center;\">"
						+ "<label id=\"rybenaLabel\" style=\"font-family: arial, helvetica; font-size: 12px; font-weight: bold; text-align: center;\">Rybená</label>"
						+ "</div>"
						+ "<div style=\"position: absolute; top: 320px; left: 58px;\">"
						+ "<img id=\"playBtn\" onclick=\"repeatRybena();\" alt=\"play\" src=\""
						+ rybenaRepositoryUrl
						+ "resource/img/play.png\" onmouseover=\"this.src='"
						+ rybenaRepositoryUrl
						+ "resource/img/playSub.png'\" onmouseout=\"this.src='"
						+ rybenaRepositoryUrl
						+ "resource/img/play.png'\" border=\"0\" title=\"Traduzir\" ondblclick=\"pauseRybena();\"> <img id=\"stopBtn\" onclick=\"stopRybena();\" alt=\"stop\" src=\""
						+ rybenaRepositoryUrl
						+ "resource/img/stop.png\" onmouseover=\"this.src='"
						+ rybenaRepositoryUrl
						+ "resource/img/stopSub.png'\" onmouseout=\"this.src='"
						+ rybenaRepositoryUrl
						+ "resource/img/stop.png'\" border=\"0\" title=\"Parar Tradução\"> <!--<img id=\"repeatBtn\" onclick=\"repeatRybena();\" alt=\"repeat\" src=\""
						+ rybenaRepositoryUrl
						+ "resource/img/repeat.png\" onmouseover=\"this.src='"
						+ rybenaRepositoryUrl
						+ "resource/img/repeatSub.png'\" onmouseout=\"this.src='"
						+ rybenaRepositoryUrl
						+ "resource/img/repeat.png'\" border=\"0\" title=\"Repetir texto já traduzido\">--> <img id=\"speedUpBtn\" onclick=\"speedUpRybena();\" alt=\"speedUp\" src=\""
						+ rybenaRepositoryUrl
						+ "resource/img/speedUp.png\" onmouseover=\"this.src='"
						+ rybenaRepositoryUrl
						+ "resource/img/speedUpSub.png'\" onmouseout=\"this.src='"
						+ rybenaRepositoryUrl
						+ "resource/img/speedUp.png'\" border=\"0\" title=\"Aumentar velocidade de tradução\"> <img id=\"speedDownBtn\" onclick=\"speedDownRybena();\" alt=\"speedDown\" src=\""
						+ rybenaRepositoryUrl
						+ "resource/img/speedDown.png\" onmouseover=\"this.src='"
						+ rybenaRepositoryUrl
						+ "resource/img/speedDownSub.png'\" onmouseout=\"this.src='"
						+ rybenaRepositoryUrl
						+ "resource/img/speedDown.png'\" border=\"0\" title=\"Diminuir velocidade de tradução\">"
						+ "</div>"
						+ "</div>"
						+ "<div id=\"rybenaVozDiv\" style=\"background-image: url('"
						+ rybenaRepositoryUrl
						+ "resource/img/borda_libras.png'); height: 358px; width: 211px; position: fixed; top: 60px; left: 570px; align: center; visibility: hidden; z-index: 9999;\">"
						+ "<div onclick=\"fecharTTS();\" title=\"Fechar\" style=\"height: 24px; width: 22px; position: relative; top: 3px; left: 182px;\"></div>"
						+ "<div style=\"position: absolute; top: 37px; left: 9px; margin-left: auto; margin-right: auto;\">"
						+ "<img id=\"rybenaVozImage\" alt=\"RybenáVoz\" src=\""
						+ rybenaRepositoryUrl
						+ "resource/img/rybeninha_voz_idle.gif\" width=\"194\" border=\"0\">"
						+ "</div>"
						+ "<div style=\"position: absolute; top: 297px; width: 211px; text-align: center;\">"
						+ "<label id=\"rybenaVozLabel\" style=\"font-family: arial, helvetica; font-size: 12px; font-weight: bold; text-align: center;\"></label>"
						+ "</div>"
						+ "<div style=\"position: absolute; left: 5px; top: 28px; width: 200px; height: 38px; text-align: center;\">"
						+ "<audio onloadstart=\"onloadTTS();\" oncanplay=\"onplayTTS();\" onended=\"pauseTTS();\" onerror=\"pauseTTS();\" id=\"tts\" hidden=\"hidden\">"
						+ "<source src=\"\" type=\"\">"
						+ "Your Browser do not support Html5+ Audio."
						+ "</audio>"
						+ "</div>"
						+ "<div style=\"position: absolute; top: 320px; left: 80px;\">"
						+ "<img id=\"playBtn\" onclick=\"playTTS();\" alt=\"play\" src=\""
						+ rybenaRepositoryUrl
						+ "resource/img/play.png\" onmouseover=\"this.src='"
						+ rybenaRepositoryUrl
						+ "resource/img/playSub.png'\" onmouseout=\"this.src='"
						+ rybenaRepositoryUrl
						+ "resource/img/play.png'\" border=\"0\" title=\"Traduzir\" ondblclick=\"playTTS();\"> <img id=\"stopBtn\" onclick=\"pauseTTS();\" alt=\"stop\" src=\""
						+ rybenaRepositoryUrl
						+ "resource/img/stop.png\" onmouseover=\"this.src='"
						+ rybenaRepositoryUrl
						+ "resource/img/stopSub.png'\" onmouseout=\"this.src='"
						+ rybenaRepositoryUrl
						+ "resource/img/stop.png'\" border=\"0\" title=\"Parar Tradução\"> <!--<img id=\"restartTTS();\" onclick=\"repeatSound();\" alt=\"repeat\" src=\""
						+ rybenaRepositoryUrl
						+ "resource/img/repeat.png\" onmouseover=\"this.src='"
						+ rybenaRepositoryUrl
						+ "resource/img/repeatSub.png'\" onmouseout=\"this.src='"
						+ rybenaRepositoryUrl
						+ "resource/img/repeat.png'\" border=\"0\" title=\"Repetir texto já traduzido\">-->"
						+ "</div>" + "</div>" + "</div>" + "</div>";

		barraStr += "<!--InicializarDragandDropRybenaJavascript -->"
						+ "<script type=\"text/javascript\">"
						+ "initialize();"
						+ "function detectPlugin() {"
						+ "name = \"java\";"
						+ "for( var i = 0; navigator.plugins[ i ]; ++i ) {"
						+ "if( navigator.plugins[ i ].name.toLowerCase().indexOf( name ) > -1 )"
						+ "   alert(navigator.plugins[ i ].name.toLowerCase());"
						+ "return true;" + "}" + "return false;"
						+ "}//document.onmouseup = startLibras" + "</script>";

		return barraStr;

	/*} else {

		// If is IE put Rybena Java instead.
		barraStr = "<!-- Início do Código do Rybená --> "
						+ "<div class=\"rybena\" />"
						+ "<link href=\"http://www.grupoicts.com.br/conf/js/jquery-ui.css\" rel=\"stylesheet\" type=\"text/css\" />"
						+ " <link href=\"http://www.grupoicts.com.br/conf/js/rybena.css\" rel=\"stylesheet\" type=\"text/css\" />"
						+ "<script src=\"http://www.grupoicts.com.br/conf/js_new/jquery.min.js\"> </script>"
						+ "<script src=\"http://www.grupoicts.com.br/conf/js_new/jquery-ui.min.js\"></script>"
						+ "<script src=\"http://www.grupoicts.com.br/conf/js/rybena.js\"></script>"
						+ " <script>"
						+ "var $j = jQuery.noConflict();"
						+ "$j(document).ready(function() {"
						+ "$j(\"#libras\").draggable();"
						+ "$j(\"#voz\").draggable();"
						+ "});"
						+ " </script>"
						+"<img width=\"135px\" height=\"37px\" usemap=\"#rybenamapTopo\" alt=\"Rybena;\" src=\""
						+ rybenaRepositoryUrl
						+ "images/barraRybena.png\" border=\"0\">"
						+ "<map id=\"rybenamapTopo\" name=\"rybenamapTopo\">"
						+ "<area style=\"cursor: pointer\" onclick=\"chamaLibras()\" title=\"Clique aqui para traduzir para LIBRAS o texto selecionado.\" alt=\"Clique aqui para traduzir para LIBRAS o texto selecionado.\" coords=\"70,0,99,29\" shape=\"rect\">"
						+ "<area style=\"cursor: pointer; background-color: Green;\" onclick=\"chamaVoz()\" title=\"Clique aqui para ouvir o texto selecionado.\" alt=\"Clique aqui para ouvir o texto selecionado.\" coords=\"105,1,138,32\" shape=\"rect\">"
						+ "</map>"
						+ "<div id=\"libras\" class=\"drag\" style=\"position:fixed; visibility: hidden; width:0px; height:0px; top: 180px; left: 84px; background-color: #FFFFFF;\"></div>"
						+ " <div id=\"voz\" class=\"drag\" style=\"position:fixed; visibility: hidden; width:0px; height:0px; top: 180px; left: 84px; background-color: #FFFFFF;\"></div>"
						+ "</div>"
						+ "    <!-- FIM Código do Rybená -->";

	}
*/
}

function includeRybena() {
	//if (getInternetExplorerVersion() == -1) {
		document
				.write("<div id=\"headerRybena\">"
						+ "<div class=\"rybena\">"
						+ "<img width=\"135px\" height=\"37px\" usemap=\"#rybenamapTopo\" alt=\"Rybena;\" src=\""
						+ rybenaRepositoryUrl
						+ "images/barraRybena.png\" border=\"0\">"
						+ "<map id=\"rybenamapTopo\" name=\"rybenamapTopo\">"
						+ "<area style=\"cursor: pointer\" onclick=\"showRybena()\" title=\"Clique aqui para traduzir para LIBRAS o texto selecionado.\" alt=\"Clique aqui para traduzir para LIBRAS o texto selecionado.\" coords=\"70,0,99,29\" shape=\"rect\">"
						+ "<area style=\"cursor: pointer; background-color: Green;\" onclick=\"showTTS()\" title=\"Clique aqui para ouvir o texto selecionado.\" alt=\"Clique aqui para ouvir o texto selecionado.\" coords=\"105,1,138,32\" shape=\"rect\">"
						+ "</map>"
						+ "<div id=\"rybenaDiv\" style=\"background-image: url('"
						+ rybenaRepositoryUrl
						+ "resource/img/borda_libras.png'); height: 358px; width: 211px; position: fixed; top: 60px; left: 570px; align: center; visibility: hidden; z-index: 9999;\">"
						+ "<div onclick=\"fecharLibras();\" title=\"Fechar\" style=\"height: 24px; width: 22px; position: relative; top: 3px; left: 182px;\"></div>"
						+ "<div style=\"position: absolute; top: 37px; left: 9px; margin-left: auto; margin-right: auto;\">"
						+ "<img id=\"rybenaImage\" alt=\"Rybená\" src=\""
						+ rybenaRepositoryUrl
						+ "resource/img/rybeninha_idle.gif\" width=\"194\" border=\"0\">"
						+ "</div>"
						+ "<div style=\"position: absolute; top: 297px; width: 211px; text-align: center;\">"
						+ "<label id=\"rybenaLabel\" style=\"font-family: arial, helvetica; font-size: 12px; font-weight: bold; text-align: center;\">Rybená</label>"
						+ "</div>"
						+ "<div style=\"position: absolute; top: 320px; left: 58px;\">"
						+ "<img id=\"playBtn\" onclick=\"repeatRybena();\" alt=\"play\" src=\""
						+ rybenaRepositoryUrl
						+ "resource/img/play.png\" onmouseover=\"this.src='"
						+ rybenaRepositoryUrl
						+ "resource/img/playSub.png'\" onmouseout=\"this.src='"
						+ rybenaRepositoryUrl
						+ "resource/img/play.png'\" border=\"0\" title=\"Traduzir\" onclick=\"setPlayRybena();\"> <img id=\"pauseBtn\" onclick=\"pauseRybena();\" alt=\"pause\" src=\""
						+ rybenaRepositoryUrl
						+ "resource/img/pause.png\" onmouseover=\"this.src='"
						+ rybenaRepositoryUrl
						+ "resource/img/pauseSub.png'\" onmouseout=\"this.src='"
						+ rybenaRepositoryUrl
						+ "resource/img/pause.png'\" border=\"0\" style=\"visibility: hidden; position: absolute; left: -1px; top: -1px; \" title=\"Pausar Tradução\"> <img id=\"stopBtn\" onclick=\"stopRybena();\" alt=\"stop\" src=\""
						+ rybenaRepositoryUrl
						+ "resource/img/stop.png\" onmouseover=\"this.src='"
						+ rybenaRepositoryUrl
						+ "resource/img/stopSub.png'\" onmouseout=\"this.src='"
						+ rybenaRepositoryUrl
						+ "resource/img/stop.png'\" border=\"0\" title=\"Parar Tradução\"> <!--<img id=\"repeatBtn\" onclick=\"repeatRybena();\" alt=\"repeat\" src=\""
						+ rybenaRepositoryUrl
						+ "resource/img/repeat.png\" onmouseover=\"this.src='"
						+ rybenaRepositoryUrl
						+ "resource/img/repeatSub.png'\" onmouseout=\"this.src='"
						+ rybenaRepositoryUrl
						+ "resource/img/repeat.png'\" border=\"0\" title=\"Repetir texto já traduzido\">--> <img id=\"speedUpBtn\" onclick=\"speedUpRybena();\" alt=\"speedUp\" src=\""
						+ rybenaRepositoryUrl
						+ "resource/img/speedUp.png\" onmouseover=\"this.src='"
						+ rybenaRepositoryUrl
						+ "resource/img/speedUpSub.png'\" onmouseout=\"this.src='"
						+ rybenaRepositoryUrl
						+ "resource/img/speedUp.png'\" border=\"0\" title=\"Aumentar velocidade de tradução\"> <img id=\"speedDownBtn\" onclick=\"speedDownRybena();\" alt=\"speedDown\" src=\""
						+ rybenaRepositoryUrl
						+ "resource/img/speedDown.png\" onmouseover=\"this.src='"
						+ rybenaRepositoryUrl
						+ "resource/img/speedDownSub.png'\" onmouseout=\"this.src='"
						+ rybenaRepositoryUrl
						+ "resource/img/speedDown.png'\" border=\"0\" title=\"Diminuir velocidade de tradução\">"
						+ "</div>"
						+ "</div>"
						+ "<div id=\"rybenaVozDiv\" style=\"background-image: url('"
						+ rybenaRepositoryUrl
						+ "resource/img/borda_libras.png'); height: 358px; width: 211px; position: fixed; top: 60px; left: 570px; align: center; visibility: hidden; z-index: 9999;\">"
						+ "<div onclick=\"fecharTTS();\" title=\"Fechar\" style=\"height: 24px; width: 22px; position: relative; top: 3px; left: 182px;\"></div>"
						+ "<div style=\"position: absolute; top: 37px; left: 9px; margin-left: auto; margin-right: auto;\">"
						+ "<img id=\"rybenaVozImage\" alt=\"RybenáVoz\" src=\""
						+ rybenaRepositoryUrl
						+ "resource/img/rybeninha_voz_idle.gif\" width=\"194\" border=\"0\">"
						+ "</div>"
						+ "<div style=\"position: absolute; top: 297px; width: 211px; text-align: center;\">"
						+ "<label id=\"rybenaVozLabel\" style=\"font-family: arial, helvetica; font-size: 12px; font-weight: bold; text-align: center;\"></label>"
						+ "</div>"
						+ "<div style=\"position: absolute; left: 5px; top: 28px; width: 200px; height: 38px; text-align: center;\">"
						+ "<audio onloadstart=\"onloadTTS();\" oncanplay=\"onplayTTS();\" onended=\"pauseTTS();\" onerror=\"pauseTTS();\" id=\"tts\" hidden=\"hidden\">"
						+ "<source src=\"\" type=\"\">"
						+ "Your Browser do not support Html5+ Audio."
						+ "</audio>"
						+ "</div>"
						+ "<div style=\"position: absolute; top: 320px; left: 80px;\">"
						+ "<img id=\"playBtnVoz\" onclick=\"playTTS();\" alt=\"play\" src=\""
						+ rybenaRepositoryUrl
						+ "resource/img/play.png\" onmouseover=\"this.src='"
						+ rybenaRepositoryUrl
						+ "resource/img/playSub.png'\" onmouseout=\"this.src='"
						+ rybenaRepositoryUrl
						+ "resource/img/play.png'\" border=\"0\" title=\"Traduzir\" ondblclick=\"playTTS();\"> <img id=\"stopBtn\" onclick=\"pauseTTS();\" alt=\"stop\" src=\""
						+ rybenaRepositoryUrl
						+ "resource/img/stop.png\" onmouseover=\"this.src='"
						+ rybenaRepositoryUrl
						+ "resource/img/stopSub.png'\" onmouseout=\"this.src='"
						+ rybenaRepositoryUrl
						+ "resource/img/stop.png'\" border=\"0\" title=\"Parar Tradução\"> <!--<img id=\"restartTTS();\" onclick=\"repeatSound();\" alt=\"repeat\" src=\""
						+ rybenaRepositoryUrl
						+ "resource/img/repeat.png\" onmouseover=\"this.src='"
						+ rybenaRepositoryUrl
						+ "resource/img/repeatSub.png'\" onmouseout=\"this.src='"
						+ rybenaRepositoryUrl
						+ "resource/img/repeat.png'\" border=\"0\" title=\"Repetir texto já traduzido\">-->"
						+ "</div>" + "</div>" + "</div>" + "</div>");

		document
				.write("<!--InicializarDragandDropRybenaJavascript -->"
						+ "<script type=\"text/javascript\">"
						+ "initialize();"
						+ "function detectPlugin() {"
						+ "name = \"java\";"
						+ "for( var i = 0; navigator.plugins[ i ]; ++i ) {"
						+ "if( navigator.plugins[ i ].name.toLowerCase().indexOf( name ) > -1 )"
						+ "   alert(navigator.plugins[ i ].name.toLowerCase());"
						+ "return true;" + "}" + "return false;"
						+ "}//document.onmouseup = startLibras" + "</script>");

	/*} else {

		// If is IE put Rybena Java instead.
		document
				.write("<!-- Início do Código do Rybená --> "
						+ "<div class=\"rybena\" />"
						+ "<link href=\"http://www.grupoicts.com.br/conf/js/jquery-ui.css\" rel=\"stylesheet\" type=\"text/css\" />"
						+ " <link href=\"http://www.grupoicts.com.br/conf/js/rybena.css\" rel=\"stylesheet\" type=\"text/css\" />"
						+ "<script src=\"http://www.grupoicts.com.br/conf/js_new/jquery.min.js\"> </script>"
						+ "<script src=\"http://www.grupoicts.com.br/conf/js_new/jquery-ui.min.js\"></script>"
						+ "<script src=\"http://www.grupoicts.com.br/conf/js/rybena.js\"></script>"
						+ " <script>"
						+ "var $j = jQuery.noConflict();"
						+ "$j(document).ready(function() {"
						+ "$j(\"#libras\").draggable();"
						+ "$j(\"#voz\").draggable();"
						+ "});"
						+ " </script>"
						+"<img width=\"135px\" height=\"37px\" usemap=\"#rybenamapTopo\" alt=\"Rybena;\" src=\""
						+ rybenaRepositoryUrl
						+ "images/barraRybena.png\" border=\"0\">"
						+ "<map id=\"rybenamapTopo\" name=\"rybenamapTopo\">"
						+ "<area style=\"cursor: pointer\" onclick=\"chamaLibras()\" title=\"Clique aqui para traduzir para LIBRAS o texto selecionado.\" alt=\"Clique aqui para traduzir para LIBRAS o texto selecionado.\" coords=\"70,0,99,29\" shape=\"rect\">"
						+ "<area style=\"cursor: pointer; background-color: Green;\" onclick=\"chamaVoz()\" title=\"Clique aqui para ouvir o texto selecionado.\" alt=\"Clique aqui para ouvir o texto selecionado.\" coords=\"105,1,138,32\" shape=\"rect\">"
						+ "</map>"
						+ "<div id=\"libras\" class=\"drag\" style=\"position:fixed; visibility: hidden; width:0px; height:0px; top: 180px; left: 84px; background-color: #FFFFFF;\"></div>"
						+ " <div id=\"voz\" class=\"drag\" style=\"position:fixed; visibility: hidden; width:0px; height:0px; top: 180px; left: 84px; background-color: #FFFFFF;\"></div>"
						+ "</div>"
						+ "    <!-- FIM Código do Rybená -->");

	}*/

}

function includeRybenaXys() {
	var img = "borda_libras.png";
	includeRybenaCustom(img);
}

function includeRybenaCustom(img) {
	document
	.write("<div id=\"headerRybena\">"
			+ "<div class=\"rybena\">"
			+ "<img width=\"135px\" height=\"37px\" usemap=\"#rybenamapTopo\" alt=\"Rybena;\" src=\""
			+ rybenaRepositoryUrl
			+ "images/barraRybena.png\" border=\"0\">"
			+ "<map id=\"rybenamapTopo\" name=\"rybenamapTopo\">"
			+ "<area style=\"cursor: pointer\" onclick=\"showRybena()\" title=\"Clique aqui para traduzir para LIBRAS o texto selecionado.\" alt=\"Clique aqui para traduzir para LIBRAS o texto selecionado.\" coords=\"70,0,99,29\" shape=\"rect\">"
			+ "<area style=\"cursor: pointer; background-color: Green;\" onclick=\"showTTS()\" title=\"Clique aqui para ouvir o texto selecionado.\" alt=\"Clique aqui para ouvir o texto selecionado.\" coords=\"105,1,138,32\" shape=\"rect\">"
			+ "</map>"
			+ "<div id=\"rybenaDiv\" style=\"background-image: url('"
			+ rybenaRepositoryUrl
			+ "resource/img/"+img+"'); height: 358px; width: 211px; position: fixed; top: 60px; left: 570px; align: center; visibility: hidden; z-index: 9999;\">"
			+ "<div onclick=\"fecharLibras();\" title=\"Fechar\" style=\"height: 24px; width: 22px; position: relative; top: 3px; left: 182px;\"></div>"
			+ "<div style=\"position: absolute; top: 37px; left: 9px; margin-left: auto; margin-right: auto;\">"
			+ "<img id=\"rybenaImage\" alt=\"Rybená\" src=\""
			+ rybenaRepositoryUrl
			+ "resource/img/rybeninha_idle.gif\" width=\"194\" border=\"0\">"
			+ "</div>"
			+ "<div style=\"position: absolute; top: 297px; width: 211px; text-align: center;\">"
			+ "<label id=\"rybenaLabel\" style=\"font-family: arial, helvetica; font-size: 12px; font-weight: bold; text-align: center;\">Rybená</label>"
			+ "</div>"
			+ "<div style=\"position: absolute; top: 320px; left: 58px;\">"
			+ "<img id=\"playBtn\" onclick=\"repeatRybena();\" alt=\"play\" src=\""
			+ rybenaRepositoryUrl
			+ "resource/img/play.png\" onmouseover=\"this.src='"
			+ rybenaRepositoryUrl
			+ "resource/img/playSub.png'\" onmouseout=\"this.src='"
			+ rybenaRepositoryUrl
			+ "resource/img/play.png'\" border=\"0\" title=\"Traduzir\" onclick=\"setPlayRybena();\"> <img id=\"pauseBtn\" onclick=\"pauseRybena();\" alt=\"pause\" src=\""
			+ rybenaRepositoryUrl
			+ "resource/img/pause.png\" onmouseover=\"this.src='"
			+ rybenaRepositoryUrl
			+ "resource/img/pauseSub.png'\" onmouseout=\"this.src='"
			+ rybenaRepositoryUrl
			+ "resource/img/pause.png'\" border=\"0\" style=\"visibility: hidden; position: absolute; left: -1px; top: -1px; \" title=\"Pausar Tradução\"> <img id=\"stopBtn\" onclick=\"stopRybena();\" alt=\"stop\" src=\""
			+ rybenaRepositoryUrl
			+ "resource/img/stop.png\" onmouseover=\"this.src='"
			+ rybenaRepositoryUrl
			+ "resource/img/stopSub.png'\" onmouseout=\"this.src='"
			+ rybenaRepositoryUrl
			+ "resource/img/stop.png'\" border=\"0\" title=\"Parar Tradução\"> <!--<img id=\"repeatBtn\" onclick=\"repeatRybena();\" alt=\"repeat\" src=\""
			+ rybenaRepositoryUrl
			+ "resource/img/repeat.png\" onmouseover=\"this.src='"
			+ rybenaRepositoryUrl
			+ "resource/img/repeatSub.png'\" onmouseout=\"this.src='"
			+ rybenaRepositoryUrl
			+ "resource/img/repeat.png'\" border=\"0\" title=\"Repetir texto já traduzido\">--> <img id=\"speedUpBtn\" onclick=\"speedUpRybena();\" alt=\"speedUp\" src=\""
			+ rybenaRepositoryUrl
			+ "resource/img/speedUp.png\" onmouseover=\"this.src='"
			+ rybenaRepositoryUrl
			+ "resource/img/speedUpSub.png'\" onmouseout=\"this.src='"
			+ rybenaRepositoryUrl
			+ "resource/img/speedUp.png'\" border=\"0\" title=\"Aumentar velocidade de tradução\"> <img id=\"speedDownBtn\" onclick=\"speedDownRybena();\" alt=\"speedDown\" src=\""
			+ rybenaRepositoryUrl
			+ "resource/img/speedDown.png\" onmouseover=\"this.src='"
			+ rybenaRepositoryUrl
			+ "resource/img/speedDownSub.png'\" onmouseout=\"this.src='"
			+ rybenaRepositoryUrl
			+ "resource/img/speedDown.png'\" border=\"0\" title=\"Diminuir velocidade de tradução\">"
			+ "</div>"
			+ "</div>"
			+ "<div id=\"rybenaVozDiv\" style=\"background-image: url('"
			+ rybenaRepositoryUrl
			+ "resource/img/"+img+"'); height: 358px; width: 211px; position: fixed; top: 60px; left: 570px; align: center; visibility: hidden; z-index: 9999;\">"
			+ "<div onclick=\"fecharTTS();\" title=\"Fechar\" style=\"height: 24px; width: 22px; position: relative; top: 3px; left: 182px;\"></div>"
			+ "<div style=\"position: absolute; top: 37px; left: 9px; margin-left: auto; margin-right: auto;\">"
			+ "<img id=\"rybenaVozImage\" alt=\"RybenáVoz\" src=\""
			+ rybenaRepositoryUrl
			+ "resource/img/rybeninha_voz_idle.gif\" width=\"194\" border=\"0\">"
			+ "</div>"
			+ "<div style=\"position: absolute; top: 297px; width: 211px; text-align: center;\">"
			+ "<label id=\"rybenaVozLabel\" style=\"font-family: arial, helvetica; font-size: 12px; font-weight: bold; text-align: center;\"></label>"
			+ "</div>"
			+ "<div style=\"position: absolute; left: 5px; top: 28px; width: 200px; height: 38px; text-align: center;\">"
			+ "<audio onloadstart=\"onloadTTS();\" oncanplay=\"onplayTTS();\" onended=\"pauseTTS();\" onerror=\"pauseTTS();\" id=\"tts\" hidden=\"hidden\">"
			+ "<source src=\"\" type=\"\">"
			+ "Your Browser do not support Html5+ Audio."
			+ "</audio>"
			+ "</div>"
			+ "<div style=\"position: absolute; top: 320px; left: 80px;\">"
			+ "<img id=\"playBtn\" onclick=\"playTTS();\" alt=\"play\" src=\""
			+ rybenaRepositoryUrl
			+ "resource/img/play.png\" onmouseover=\"this.src='"
			+ rybenaRepositoryUrl
			+ "resource/img/playSub.png'\" onmouseout=\"this.src='"
			+ rybenaRepositoryUrl
			+ "resource/img/play.png'\" border=\"0\" title=\"Traduzir\" ondblclick=\"playTTS();\"> <img id=\"stopBtn\" onclick=\"pauseTTS();\" alt=\"stop\" src=\""
			+ rybenaRepositoryUrl
			+ "resource/img/stop.png\" onmouseover=\"this.src='"
			+ rybenaRepositoryUrl
			+ "resource/img/stopSub.png'\" onmouseout=\"this.src='"
			+ rybenaRepositoryUrl
			+ "resource/img/stop.png'\" border=\"0\" title=\"Parar Tradução\"> <!--<img id=\"restartTTS();\" onclick=\"repeatSound();\" alt=\"repeat\" src=\""
			+ rybenaRepositoryUrl
			+ "resource/img/repeat.png\" onmouseover=\"this.src='"
			+ rybenaRepositoryUrl
			+ "resource/img/repeatSub.png'\" onmouseout=\"this.src='"
			+ rybenaRepositoryUrl
			+ "resource/img/repeat.png'\" border=\"0\" title=\"Repetir texto já traduzido\">-->"
			+ "</div>" + "</div>" + "</div>" + "</div>");

document
	.write("<!--InicializarDragandDropRybenaJavascript -->"
			+ "<script type=\"text/javascript\">"
			+ "initialize();"
			+ "function detectPlugin() {"
			+ "name = \"java\";"
			+ "for( var i = 0; navigator.plugins[ i ]; ++i ) {"
			+ "if( navigator.plugins[ i ].name.toLowerCase().indexOf( name ) > -1 )"
			+ "   alert(navigator.plugins[ i ].name.toLowerCase());"
			+ "return true;" + "}" + "return false;"
			+ "}//document.onmouseup = startLibras" + "</script>");
}

function includeRybenaNoBar(){
	document
	.write("<div id=\"rybenaDiv\" style=\"background-image: url('"
			+ rybenaRepositoryUrl
			+ "resource/img/"+img+"'); height: 358px; width: 211px; position: fixed; top: 60px; left: 570px; align: center; visibility: hidden; z-index: 9999;\">"
			+ "<div onclick=\"fecharLibras();\" title=\"Fechar\" style=\"height: 24px; width: 22px; position: relative; top: 3px; left: 182px;\"></div>"
			+ "<div style=\"position: absolute; top: 37px; left: 9px; margin-left: auto; margin-right: auto;\">"
			+ "<img id=\"rybenaImage\" alt=\"Rybená\" src=\""
			+ rybenaRepositoryUrl
			+ "resource/img/rybeninha_idle.gif\" width=\"194\" border=\"0\">"
			+ "</div>"
			+ "<div style=\"position: absolute; top: 297px; width: 211px; text-align: center;\">"
			+ "<label id=\"rybenaLabel\" style=\"font-family: arial, helvetica; font-size: 12px; font-weight: bold; text-align: center;\">Rybená</label>"
			+ "</div>"
			+ "<div style=\"position: absolute; top: 320px; left: 58px;\">"
			+ "<img id=\"playBtn\" onclick=\"repeatRybena();\" alt=\"play\" src=\""
			+ rybenaRepositoryUrl
			+ "resource/img/play.png\" onmouseover=\"this.src='"
			+ rybenaRepositoryUrl
			+ "resource/img/playSub.png'\" onmouseout=\"this.src='"
			+ rybenaRepositoryUrl
			+ "resource/img/play.png'\" border=\"0\" title=\"Traduzir\" onclick=\"setPlayRybena();\"> <img id=\"pauseBtn\" onclick=\"pauseRybena();\" alt=\"pause\" src=\""
			+ rybenaRepositoryUrl
			+ "resource/img/pause.png\" onmouseover=\"this.src='"
			+ rybenaRepositoryUrl
			+ "resource/img/pauseSub.png'\" onmouseout=\"this.src='"
			+ rybenaRepositoryUrl
			+ "resource/img/pause.png'\" border=\"0\" style=\"visibility: hidden; position: absolute; left: -1px; top: -1px; \" title=\"Pausar Tradução\"> <img id=\"stopBtn\" onclick=\"stopRybena();\" alt=\"stop\" src=\""
			+ rybenaRepositoryUrl
			+ "resource/img/stop.png\" onmouseover=\"this.src='"
			+ rybenaRepositoryUrl
			+ "resource/img/stopSub.png'\" onmouseout=\"this.src='"
			+ rybenaRepositoryUrl
			+ "resource/img/stop.png'\" border=\"0\" title=\"Parar Tradução\"> <!--<img id=\"repeatBtn\" onclick=\"repeatRybena();\" alt=\"repeat\" src=\""
			+ rybenaRepositoryUrl
			+ "resource/img/repeat.png\" onmouseover=\"this.src='"
			+ rybenaRepositoryUrl
			+ "resource/img/repeatSub.png'\" onmouseout=\"this.src='"
			+ rybenaRepositoryUrl
			+ "resource/img/repeat.png'\" border=\"0\" title=\"Repetir texto já traduzido\">--> <img id=\"speedUpBtn\" onclick=\"speedUpRybena();\" alt=\"speedUp\" src=\""
			+ rybenaRepositoryUrl
			+ "resource/img/speedUp.png\" onmouseover=\"this.src='"
			+ rybenaRepositoryUrl
			+ "resource/img/speedUpSub.png'\" onmouseout=\"this.src='"
			+ rybenaRepositoryUrl
			+ "resource/img/speedUp.png'\" border=\"0\" title=\"Aumentar velocidade de tradução\"> <img id=\"speedDownBtn\" onclick=\"speedDownRybena();\" alt=\"speedDown\" src=\""
			+ rybenaRepositoryUrl
			+ "resource/img/speedDown.png\" onmouseover=\"this.src='"
			+ rybenaRepositoryUrl
			+ "resource/img/speedDownSub.png'\" onmouseout=\"this.src='"
			+ rybenaRepositoryUrl
			+ "resource/img/speedDown.png'\" border=\"0\" title=\"Diminuir velocidade de tradução\">"
			+ "</div>"
			+ "</div>"
			+ "<div id=\"rybenaVozDiv\" style=\"background-image: url('"
			+ rybenaRepositoryUrl
			+ "resource/img/"+img+"'); height: 358px; width: 211px; position: fixed; top: 60px; left: 570px; align: center; visibility: hidden; z-index: 9999;\">"
			+ "<div onclick=\"fecharTTS();\" title=\"Fechar\" style=\"height: 24px; width: 22px; position: relative; top: 3px; left: 182px;\"></div>"
			+ "<div style=\"position: absolute; top: 37px; left: 9px; margin-left: auto; margin-right: auto;\">"
			+ "<img id=\"rybenaVozImage\" alt=\"RybenáVoz\" src=\""
			+ rybenaRepositoryUrl
			+ "resource/img/rybeninha_voz_idle.gif\" width=\"194\" border=\"0\">"
			+ "</div>"
			+ "<div style=\"position: absolute; top: 297px; width: 211px; text-align: center;\">"
			+ "<label id=\"rybenaVozLabel\" style=\"font-family: arial, helvetica; font-size: 12px; font-weight: bold; text-align: center;\"></label>"
			+ "</div>"
			+ "<div style=\"position: absolute; left: 5px; top: 28px; width: 200px; height: 38px; text-align: center;\">"
			+ "<audio onloadstart=\"onloadTTS();\" oncanplay=\"onplayTTS();\" onended=\"pauseTTS();\" onerror=\"pauseTTS();\" id=\"tts\" hidden=\"hidden\">"
			+ "<source src=\"\" type=\"\">"
			+ "Your Browser do not support Html5+ Audio."
			+ "</audio>"
			+ "</div>"
			+ "<div style=\"position: absolute; top: 320px; left: 80px;\">"
			+ "<img id=\"playBtn\" onclick=\"playTTS();\" alt=\"play\" src=\""
			+ rybenaRepositoryUrl
			+ "resource/img/play.png\" onmouseover=\"this.src='"
			+ rybenaRepositoryUrl
			+ "resource/img/playSub.png'\" onmouseout=\"this.src='"
			+ rybenaRepositoryUrl
			+ "resource/img/play.png'\" border=\"0\" title=\"Traduzir\" ondblclick=\"playTTS();\"> <img id=\"stopBtn\" onclick=\"pauseTTS();\" alt=\"stop\" src=\""
			+ rybenaRepositoryUrl
			+ "resource/img/stop.png\" onmouseover=\"this.src='"
			+ rybenaRepositoryUrl
			+ "resource/img/stopSub.png'\" onmouseout=\"this.src='"
			+ rybenaRepositoryUrl
			+ "resource/img/stop.png'\" border=\"0\" title=\"Parar Tradução\"> <!--<img id=\"restartTTS();\" onclick=\"repeatSound();\" alt=\"repeat\" src=\""
			+ rybenaRepositoryUrl
			+ "resource/img/repeat.png\" onmouseover=\"this.src='"
			+ rybenaRepositoryUrl
			+ "resource/img/repeatSub.png'\" onmouseout=\"this.src='"
			+ rybenaRepositoryUrl
			+ "resource/img/repeat.png'\" border=\"0\" title=\"Repetir texto já traduzido\">-->"
			+ "</div>" + "</div>" );

document
	.write("<!--InicializarDragandDropRybenaJavascript -->"
			+ "<script type=\"text/javascript\">"
			+ "initialize();"
			+ "function detectPlugin() {"
			+ "name = \"java\";"
			+ "for( var i = 0; navigator.plugins[ i ]; ++i ) {"
			+ "if( navigator.plugins[ i ].name.toLowerCase().indexOf( name ) > -1 )"
			+ "   alert(navigator.plugins[ i ].name.toLowerCase());"
			+ "return true;" + "}" + "return false;"
			+ "}//document.onmouseup = startLibras" + "</script>");
}
