fenapaesApp.cC({
	name: 'homeCtrl', 
	inject: ['$scope','$resource','dialogs','$sce','DataService'],
	init: function() {
		this.$.slides = [
			{
				image: '/web/img/olimpiadas.jpg',
				href: '#/artigo/24089',
				blank: false
			},
			{
				image: '/web/img/inclusao.jpg',
				href: '#/artigo/23992',
				blank: false
			},
			{
				image: '/web/img/marcaapae.jpg',
				href: 'http://www.apaesp.org.br/SaibaComoAjudar/SouPessoaJuridica/Paginas/Produtos-Institucionais.aspx',
				blank: true
			},
			{
				image: '/web/img/natal.jpg',
				href: '#/artigo/23976',
				blank: false
			},
			{
				image: '/web/img/cartao2015.jpg',
				href: '#/artigo/23928',
				blank: false
			},
			{
				image: '/web/img/cursos.jpg',
				href: 'http://cursos.uniapae.org.br/',
				blank: true
			},
			{
				image: '/web/img/embaixador.jpg',
				href: '#/artigo/23551',
				blank: false
			},
			{
				image: '/web/img/marco.jpg',
				href: '#/artigo/23959',
				blank: false
			},
			{
				image: '/web/img/twitter.jpg',
				href: 'https://twitter.com/brasilfenapaes',
				blank: true
			}
		];
		
		this.Noticia = this.$resource('/api/noticia/:id.json');
		this.Banner = this.$resource('/api/banner/:id.json');
		this.Video = this.$resource('/api/video/:id.json');

		this.$.loading = {
			noticias: false,
			banners: false,
			videos: false
		};
		
		if (this.DataService.Entidade) {
			this._startApp();
		}
		
	},
	on: {
		EntidadeLoaded: function() {
			this._startApp();
		}
	},
	methods: {
		_startApp: function() {
			this.emDestaque();
			this._banners();
			this._videos();
		},
		trustSrc: function(src) {
			return this.$sce.trustAsResourceUrl(src);
		},
		_handleLinks: function() {
			/*arq_links = $('a[href*="arquivo.phtml?a="]');
			arq_links.each(function(link){
				href = $(arq_links[link]).attr('href');
				href = href.split('=');
				nhref = '#/arquivo/'+href[1];
				$(arq_links[link]).attr('href',nhref);
			});
			*/

			art_links = $('a[href*="artigo.phtml?a="]');

			art_links.each(function(link){
				href = $(art_links[link]).attr('href');
				href = href.split('=');
				nhref = '#/artigo/'+href[1];
				$(art_links[link]).attr('href',nhref);
			});
			
			
			art_links = $('a[href*="artigo.phtml/"]');
			
			art_links.each(function(link){
				href = $(art_links[link]).attr('href');
				href = href.split('/');
				nhref = '#/artigo/'+href[href.length-1];
				$(art_links[link]).attr('href',nhref);
			});
		},
		linkBanner: function(slide) {
			link = slide.BannerImg[0].bai_link.split('/');
			link_externo = slide.BannerImg[0].bai_link;

			if (link[3] == 'artigo.phtml') {
				location.href = '#/artigo/'+link[4];
			} else if (link[3] == 'noticia.phtml') {
				location.href = '#/noticia/'+link[4];
			} else {
				location.href = link_externo;
			}
		},
		emDestaque: function() {
			query = {
				q: 'Noticia.not_aprovado.eq.1,Noticia.not_destaque.eq.1,Noticia.not_ent_id.eq.'+this.DataService.Entidade.ent_id,
				limit: 8,
				sort: '-not_data'
			};
			this.$.newsType = 'destaque';
			this._noticias(query);
		},
		asUltimas: function() {
			query = {
				q: 'Noticia.not_aprovado.eq.1,Noticia.not_ent_id.eq.1',
				limit: 20,
				sort: '-not_data'
			};
			this.$.newsType = 'ultimas';
			this._noticias(query);
		},
		todasNoticias: function() {
			location.href = '#/noticia';
		},
		_noticias: function(query) {
			this.$.loading.noticias = true;
			this.Noticia.get(query).$promise.
			then(function(data){
				this.$.Noticias = data.data;
				this.$.loading.noticias = false;
			}.bind(this));
			
		},
		_banners: function() {
			this.$.loading.banners = true;
			query = {
				q: 'Banner.ban_ent_id.eq.1,Banner.ban_ativo.eq.'+this.DataService.Entidade.ent_id,
				populate: 'BannerImg',
				limit: 7
			};
			this.Banner.get(query).$promise.
			then(function(data){
				this.$.Banners = data.data;
				this._handleLinks();
				this.$.loading.banners = false;
			}.bind(this));
		},
		_videos: function() {
			this.$.loading.videos = true;
			query = {
				q: 'Video.vid_ent_id.eq.'+this.DataService.Entidade.ent_id,
				sort: '-vid_data',
				limit: 1
			};
			this.Video.get(query).$promise.
			then(function(data){
				Video = data.data[0].Video;
				this.$.lastVideo = 'https://www.youtube.com/embed/'+Video.vid_link+'?rel=0';
				//console.log(this.$.lastVideo);
				this.$.loading.videos = false;
			}.bind(this));
		}
	}
});
