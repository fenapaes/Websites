fenapaesApp.cC({
	name: 'noticiaCtrl', 
	inject: ['$scope','$resource','dialogs','$routeParams','DataService','$location'],
	init: function() {
		this.Noticia = this.$resource('/api/noticia/:id/:year.json');
		this.$.paginator = {
			page: 1,
			ano: 2015
		};
		this.$.host = location.host;
		
		this._load();
	},
	watch: {
		'{object}paginator.page':'_load'
	},
	methods: {
		_load: function() {
			this.$.loading = true;
			this.Noticia.get(
				{
					q: 'Noticia.not_ent_id.eq.1',
					sort: '-not_data',
					page: this.$.paginator.page,
					ano: this.$.paginator.ano
				}
			).$promise.
			then(function(data){
				this.$.noticias = data.data;
				data.pagination.ano = this.$.paginator.ano;
				this.$.paginator = data.pagination;
				this.$.loading = false;
			}.bind(this));
		}
	}
});
fenapaesApp.cC({
	name: 'noticiaYearCtrl', 
	inject: ['$scope','$resource','dialogs','$routeParams','DataService','$location'],
	init: function() {
		this.Noticia = this.$resource('/api/noticia/:id.json');
		this.$.paginator = {
			page: 1,
			ano: this.$routeParams['year']
		};
		this.$.host = location.host;
		
		this._load();
	},
	watch: {
		'{object}paginator.page':'_load'
	},
	methods: {
		_load: function() {
			this.$.loading = true;
			this.Noticia.get(
				{
					q: 'Noticia.not_ent_id.eq.1,year(Noticia.not_data).eq.'+this.$routeParams['year'],
					sort: '-not_data',
					page: this.$.paginator.page,
					ano: this.$.paginator.ano
				}
			).$promise.
			then(function(data){
				this.$.noticias = data.data;
				data.pagination.ano = this.$.paginator.ano;
				this.$.paginator = data.pagination;
				this.$.loading = false;
			}.bind(this));
		}
	}
});
fenapaesApp.cC({
	name: 'noticiaViewCtrl', 
	inject: ['$scope','$resource','dialogs','$routeParams','DataService','$location','$sce'],
	init: function() {
		this.Noticia = this.$resource('/api/noticia/:id.json');
		this.$.host = location.host;
		this._view();
	},
	methods: {
		to_trusted: function(html_code) {
			return this.$sce.trustAsHtml(html_code);
		},
		listing: function() {
			this.$location.path('/noticia');
		},
		_view: function() {
			this.$.loading = true;
			this.Noticia.get(
				{
					id: this.$routeParams.id,
					populate: 'Anexo,Album'
				}
			).$promise.
			then(function(data){
				data.data.Noticia.not_conteudo = this.DataService.handleLinks(data.data.Noticia.not_conteudo);
				this.$.noticia = data.data;
				this.$.loading = false;
			}.bind(this));
		}
	}
});
