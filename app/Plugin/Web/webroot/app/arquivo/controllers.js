fenapaesApp.cC({
	name: 'arquivoCtrl', 
	inject: ['$scope','$resource','dialogs','$routeParams'],
	init: function() {
		this.Arquivo = this.$resource('/api/arquivo/:id.json');
		this.$.paginator = {
			page: 1
		};
		this._load();
	},
	watch: {
		'{object}paginator.page':'_load'
	},
	methods: {
		_load: function() {
			
			this.Artigo.get(
				{
					q: 'Arquivo.arq_ent_id.eq.1',
					page: this.$.paginator.page
				}
			).$promise.
			then(function(data){
				this.$.arquivos = data.data;
				this.$.paginator = data.paginator;
			}.bind(this));
		}
	}
});

fenapaesApp.cC({
	name: 'arquivoDownloadCtrl', 
	inject: ['$scope','$resource','dialogs','$routeParams'],
	init: function() {
		this.$.hidePage = true;
		this._download();
	},
	methods: {
		_download: function() {
			location.href = 'http://apaebrasil.org.br/arquivo.phtml?a='+this.$routeParams['id'];
		}
	}
});

fenapaesApp.cC({
	name: 'arquivoListCtrl', 
	inject: ['$scope','$resource','dialogs','$routeParams'],
	init: function() {
		this.Arquivo = this.$resource('/api/arquivo/:id.json');
		this._list();
	},
	methods: {
		_list: function() {
			this.Arquivo.get(
				{
					q: 'Arquivo.arq_art_id.eq.'+this.$routeParams.id,
					limit: 100,
					populate: 'ArquivoTop'
				}
			).$promise.
			then(function(data){
				this.$.arquivos = data.data;
			}.bind(this));
		}
	}
});

fenapaesApp.cC({
	name: 'arquivoViewCtrl', 
	inject: ['$scope','$resource','dialogs','$routeParams'],
	init: function() {
		this.Arquivo = this.$resource('/api/arquivo/:id.json');
		this._view();
	},
	watch: {
		'{object}arquivo':'_handleLinks()'
	},
	methods: {
		_handleLinks: function() {
			arq_links = $('a[href*="arquivo.phtml?a="]');
			arq_links.each(function(link){
				href = $(arq_links[link]).attr('href');
				href = href.split('=');
				nhref = '#/arquivo/'+href[1];
				$(arq_links[link]).attr('href',nhref);
			});

			art_links = $('a[href*="artigo.phtml?a="]');

			art_links.each(function(link){
				href = $(art_links[link]).attr('href');
				href = href.split('=');
				nhref = '#/artigo/'+href[1];
				$(art_links[link]).attr('href',nhref);
			});
			
			
			art_links = $('a[href*="artigo.phtml/"]');
			
			art_links.each(function(link){
				href = $(art_links[link]).attr('href');
				href = href.split('/');
				nhref = '#/artigo/'+href[href.length-1];
				$(art_links[link]).attr('href',nhref);
			});
		},
		_download: function() {
			this.Arquivo.get(
				{
					id: this.$routeParams.id
				}
			).$promise.
			then(function(data){
				this.$.arquivo = data.data;
				
				setTimeout(function(){
					this._handleLinks();
				}.bind(this), 1000);
			}.bind(this));
		}
	}
});
