fenapaesApp.cC({
	name: 'menuCtrl', 
	inject: ['$scope','$resource','dialogs'],
	init: function() {
		this.Menus = this.$resource('/api/menu/menus.json');
		
		this._loadTopMenus();
		this._loadBottomMenus();
	},
	methods: {
		_processLinks: function(links) {
			angular.forEach(links, function(value, key){
				value.Menu.men_link = this._findType(value.Menu.men_link);
				
				if (value.Submenu1.length > 0) {
					angular.forEach(value.Submenu1, function(value2, key2){
						value2.men_link = this._findType(value2.men_link);
					
						if (value2.Submenu2.length > 0) {
							angular.forEach(value2.Submenu2, function(value3, key3){
							
								value3.men_link = this._findType(value3.men_link);
							
							}.bind(this));
						}
					
					}.bind(this));
				}
			}.bind(this));

			return links;
		},
		_findType: function(link) {
			
			if (link == undefined) return '';
			
			if (link.search('artigo') > -1) {
				var values = link.split('=');
				if (values.length == 1) {
					values = link.split('/');
				}
				return '#/artigo/'+values[values.length-1];
			}
			if (link.search('noticia') > -1) {
				var values = link.split('=');
				if (values.length == 1) {
					values = link.split('/');
				}
				return '#/noticia/'+values[values.length-1];
			}
			if (link.search('arquivo') > -1) {
				var values = link.split('=');
				var types = values[0].split('?');
				if (types[types.length-1] == 't') {
					return '#/arquivo/list/'+values[values.length-1];					
				} else {
					return '/arquivo/'+values[values.length-1];
				}
			}
			if (link.search('http://') == 0) {
				return link;
			}
			return '';
		},
		_loadTopMenus: function() {
			// Carrega Menu horizontal superior
			this.Menus.get(
				{
					men_tipo: 'h',
					men_ent_id: 1,
					sort: '-men_posicao'
				}
			).$promise.
			then(function(data){
				this.$.topMenus = this._processLinks(data.data);
				var t = window.setTimeout(function(){
					$('#main-menu').smartmenus({
						keepHighlighted: true,
						subIndicatorsPos: 'append',
						subMenusSubOffsetX: 1,
						subMenusSubOffsetY: -8
					});
				}, 1000);
				
			}.bind(this));
		},
		_loadBottomMenus: function() {
			// Carrega Menu vertical lateral
			this.Menus.get(
				{
					men_tipo: 'v',
					men_ent_id: 1
				}
			).$promise.
			then(function(data){
				this.$.bottomMenus = this._processLinks(data.data);
				var s = window.setTimeout(function(){
					
					$('#side-menu').smartmenus({
						keepHighlighted: true,
						subIndicatorsPos: 'append',
						subMenusSubOffsetX: 1,
						subMenusSubOffsetY: -8
					});
				}, 1000);
				
			}.bind(this));
		}
	}
});
