fenapaesApp.cC({
	name: 'artigoCtrl', 
	inject: ['$scope','$resource','dialogs','$routeParams','DataService'],
	init: function() {
		this.Artigo = this.$resource('/api/artigo/:id.json');
		this.$.paginator = {
			page: 1
		};
		this._load();
	},
	watch: {
		'{object}paginator.page':'_load'
	},
	methods: {
		_load: function() {
			
			this.Artigo.get(
				{
					q: 'Artigo.sec_ent_id.eq.1',
					page: this.$.paginator.page
				}
			).$promise.
			then(function(data){
				this.$.artigos = data.data;
				this.$.paginator = data.paginator;
			}.bind(this));
		}
	}
});

fenapaesApp.cC({
	name: 'artigoViewCtrl', 
	inject: ['$scope','$resource','dialogs','$routeParams','DataService','$sce'],
	init: function() {
		this.Artigo = this.$resource('/api/artigo/:id.json');
		//console.log(this.DataService);
		this._view();
	},
	methods: {
		to_trusted: function(html_code) {
			return this.$sce.trustAsHtml(html_code);
		},
		_view: function() {
			this.$.loading = true;
			this.Artigo.get(
				{
					id: this.$routeParams.id
				}
			).$promise.
			then(function(data){
				data.data.Artigo.sec_conteudo = this.DataService.handleLinks(data.data.Artigo.sec_conteudo);
				this.$.artigo = data.data;
				this.$.loading = false;
			}.bind(this));
		}
	}
});
