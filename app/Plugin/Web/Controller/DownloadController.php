<?php
	class DownloadController extends WebAppController {

		public function video($file = 0) {
			$videosPath = APP.'webroot/videos/';
			$files = array(
				'',
				'semana_nacional_2015.mp4',
				'dominios.pdf'
			);
			
			$video = file_get_contents($videosPath.$files[$file]);
			
			header('Content-Description: File Transfer');
			header('Content-Disposition: attachment; filename="'.$files[$file].'"');
			header('Content-Type: application/octet-stream');
			header('Content-Transfer-Encoding: binary');
			header('Content-Length: ' . filesize($videosPath.$files[$file]));
			header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
			header('Pragma: public');
			header('Expires: 0');
			
			readfile($videosPath.$files[$file]);
			
			//$this->layout = 'ajax';
			
			$this->render(false);
			
		}

	}
