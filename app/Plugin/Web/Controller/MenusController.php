<?php
class MenusController extends WebAppController {
	
	public $components = array('RequestHandler');

	public $uses = array('Web.Entidade','Web.Menu','Web.Menu1','Web.Menu2','Web.Menu3');
	
	public function novoMenu2($oldMenu, $ent_id, $tipo_id) {
		$newMenu = array(
			'men_ent_id' => $ent_id,
			'men_posicao' => $oldMenu['me2_posicao'],
			'men_tipo' => $tipo_id,
			'men_titulo' => $oldMenu['me2_titulo'],
			'men_link' => $oldMenu['me2_link'],
			'men_novapagina' => $oldMenu['me2_novapagina'],
			'men_fixo' => $oldMenu['me2_fixo']
		);
		//pr($newMenu);
		return $newMenu;
	}
	
	public function novoMenu3($oldMenu, $ent_id, $tipo_id) {
		$newMenu = array(
			'men_ent_id' => $ent_id,
			'men_posicao' => $oldMenu['me3_posicao'],
			'men_tipo' => $tipo_id,
			'men_titulo' => $oldMenu['me3_titulo'],
			'men_link' => $oldMenu['me3_link'],
			'men_novapagina' => $oldMenu['me3_novapagina'],
			'men_fixo' => $oldMenu['me3_fixo']
		);
		return $newMenu;
	}
	
	public function ajusta() {
		
		$this->layout = 'Web.theme';
		
		$this->Entidade->Behaviors->attach('Containable');
		$this->Entidade->contain();
		
		$this->Menu1->Behaviors->attach('Containable');
		$this->Menu1->contain('Menu2','Menu2.Menu3');
		
		$entidades = $this->Entidade->find('all', array('limit'=>'1'));
		
		foreach($entidades as $ent) {
			$menus = $this->Menu1->find('all', array(
				'conditions' => array(
					'Menu1.men_ent_id' => $ent['Entidade']['ent_id']
				),
				'order' => array(
					'Menu1.men_id'
				)
			));
			$newMenu = array(
			);
			foreach ($menus as $menu1) {
				$tempMenu1 = $menu1['Menu1'];
				$tipo_id = $tempMenu1['men_tipo'];
				unset($tempMenu1['Menu2']);
				unset($tempMenu1['men_id']);
				array_push($newMenu, array('Menu' => $tempMenu1));
				if (count($menu1['Menu2']) > 0) {
					$newMenu[count($newMenu)-1]['Menu']['Submenu1'] = array();
					foreach($menu1['Menu2'] as $menu2) {
						$tempMenu2 = $this->novoMenu2($menu2, $ent['Entidade']['ent_id'], $tipo_id);
						array_push($newMenu[count($newMenu)-1]['Menu']['Submenu1'], $tempMenu2);
						if (count($newMenu[count($newMenu)-1]['Menu']['Submenu1']) > 0) { 
							$newMenu[count($newMenu)-1]['Menu']['Submenu1'][count($newMenu[count($newMenu)-1]['Menu']['Submenu1'])-1]['Submenu2'] = array();
							foreach($menu2['Menu3'] as $menu3) {
								$tempMenu3 = $this->novoMenu3($menu3, $ent['Entidade']['ent_id'], $tipo_id);
								array_push($newMenu[count($newMenu)-1]['Menu']['Submenu1'][count($newMenu[count($newMenu)-1]['Menu']['Submenu1'])-1]['Submenu2'], $tempMenu3);
							}
						}
					}
				}
			}
			//pr($newMenu);
			$this->Menu->saveMany($newMenu, array(
				'atomic'=>true,
				'deep'=>true
			));
		}

	}
	
	public function index() {
		
		$query = $this->request->query;
		
		if (!isset($query['ent_id'])) {
			$query['ent_id'] = 1;
		}
		if (!isset($query['men_tipo'])) {
			$query['men_tipo'] = 'h';
		}
		
		$this->Menu->Behaviors->attach('Containable');
		$this->Menu->contain();
				
		$data = $this->Menu->find('threaded', array(
			'limit' => 100,
			'conditions' => array(
				'Menu.men_ent_id' => $query['ent_id'],
				'OR' => array(
					'Menu.men_tipo' => $query['men_tipo'],
					'Menu.men_tipo IS NULL'
				)
			),
			'order' => array(
				'Menu.men_posicao' => 'ASC',
				'Menu.men_titulo' => 'ASC'
			)
		));
		
		$this->set('data', $data);
		$this->set('_serialize', array( 'data') );
		
		$this->render(false);
		
	}
	
	
}

