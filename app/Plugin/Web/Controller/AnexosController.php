<?php
	class AnexosController extends WebAppController {
		
		public $uses = array('Web.Anexo');

		public function download($id = null) {

			$this->layout = 'image';

			$this->REPOSITORIO = '/home/webmaster/repositorio';
			$this->Anexo->Behaviors->attach('Containable');
			$this->Anexo->contain('Entidade');
			$arqData = $this->Anexo->read(null, $id);
			
			$foto_data = split('-', $arqData['Arquivo']['arq_data']);
			$arquivo = $this->REPOSITORIO.'/web_anexo-arquivo'.'/'
				.$arqData['Entidade']['ent_fed_uf'].'/'
				.$foto_data[0].'/'
				.$arqData['Anexo']['ana_ent_id'].'/'.$arqData['Anexo']['ana_url'];
			if (is_file($arquivo)) {
				$foto = imagecreatefromjpeg($arquivo);
				$dw = 720;
				$dh = imagesy($foto)*648/imagesx($foto);
				
				if (imagesx($foto) > $dw) {
					$dst_foto = imagecreatetruecolor(648, $dh);
					imagecopyresampled($dst_foto, $foto, 0, 0, 0, 0, 648, $dh, imagesx($foto), imagesy($foto));
					$foto = $dst_foto;
				}
			} else {
				$foto = imagecreate(100, 100);
			}
			header("Content-type: image/jpeg");
			imagejpeg($foto, '', 80);
			$this->render(false);
		}
		
		public function view($id = null) {
			$this->layout = 'image';
			$this->REPOSITORIO = '/home/webmaster/repositorio';
			$this->Anexo->Behaviors->attach('Containable');
			$this->Anexo->contain('Entidade');
			$arqData = $this->Anexo->read(null, $id);
			$uf = ($arqData['Entidade']['ent_fed_uf'])?($arqData['Entidade']['ent_fed_uf']):('DF');
			$ent_id = ($arqData['Anexo']['ana_ent_id'])?($arqData['Anexo']['ana_ent_id']):('1');
			$foto_data = split('-', $arqData['Anexo']['ana_data']);
			
			$arquivo = $this->REPOSITORIO.'/web_anexo-arquivo'.'/'
				.$uf.'/'
				.$foto_data[0].'/'
				.$ent_id.'/'
				.iconv('UTF-8', 'ISO8859-1', $arqData['Anexo']['ana_url']);
			//echo $arquivo;
			if (is_file($arquivo)) {
				$tipo = mime_content_type($arquivo);
				//echo filesize($arquivo);
				if ($tipo) {
					header('Content-type:'.$tipo);
					header('Content-Disposition:'.' filename="'.$arqData['Anexo']['ana_url'].'";');
					header('Content-Length: '.filesize($arquivo));
					readfile($arquivo);
				} else {
					header("HTTP/1.0 404 Not Found");
				}
			}
			$this->render(false);
		}
		
		public function view3($id = null) {

			$this->layout = 'image';

			$this->REPOSITORIO = '/home/webmaster/repositorio';
			$this->Arquivo->Behaviors->attach('Containable');
			$this->Arquivo->contain('Entidade');
			$arqData = $this->Arquivo->read(null, $id);
			
			$foto_data = split('-', $arqData['Arquivo']['arq_data']);
			$arquivo = $this->REPOSITORIO.'/web_arquivo'.'/'
				.$arqData['Entidade']['ent_fed_uf'].'/'
				.$foto_data[0].'/'
				.$arqData['Arquivo']['arq_ent_id'];
			$file = $this->Attachment->getFile($id);
			$this->response->file($file[$arquivo]);
			// Return response object to prevent controller from trying to render
			// a view
			return $this->response;
		}
		
		public function view2($id = null, $size = 'm') {
			
			$this->layout = 'image';
			
			$image = @imagecreatefromjpeg('http://apaeminas.org.br/foto.phtml/'.$id.'/'.$size);
			if (!$image) {
				$image = @imagecreatefromgif('http://apaeminas.org.br/foto.phtml/'.$id.'/'.$size);				
			}
			if (!$image) {
				$image = @imagecreatefrompng('http://apaeminas.org.br/foto.phtml/'.$id.'/'.$size);				
			}
			header('Content-type:image/png');
			
			echo imagejpeg($image);
			
			$this->render(false);
			
		}


		
	}
