<?php
	class FotosController extends WebAppController {

		public function view($id = null, $tamanho = 'g') {

			$this->layout = 'image';
			$this->autoRender = false;

			$this->REPOSITORIO = '/home/webmaster/repositorio';

			//$tamanho = 'album';
			
			if ($tamanho == 'mini' or $tamanho == 't') $tamanho = 'thumb';
			if ($tamanho == 'p' or $tamanho == 'peq' or $tamanho == 'pequena') $tamanho = 'album2';
			if ($tamanho == 'm' or $tamanho == 'med' or $tamanho == 'media') $tamanho = 'album';
			if ($tamanho == 'g' or $tamanho == 'grande') $tamanho = 'foto';
			
			$fotoData = $this->Foto->read(null, $id);
			$foto_data = split('-', $fotoData['Foto']['fot_data']);
			$arquivo = $this->REPOSITORIO.'/web_foto'.'/'
				.$fotoData['Foto']['fot_fed_uf'].'/'
				.$foto_data[0].'/'
				.$fotoData['Foto']['fot_ent_id'].'/'.sprintf("%09d.%s",$id,$tamanho);
			//echo $arquivo;
			if (is_file($arquivo)) {
				$foto = imagecreatefromjpeg($arquivo);
				$dw = 720;
				$dh = imagesy($foto)*648/imagesx($foto);
				
				if (imagesx($foto) > $dw) {
					$dst_foto = imagecreatetruecolor(648, $dh);
					imagecopyresampled($dst_foto, $foto, 0, 0, 0, 0, 648, $dh, imagesx($foto), imagesy($foto));
					$foto = $dst_foto;
				}
			} else {
				$foto = imagecreate(100, 100);
			}
			header("Content-type: ".$fotoData['Foto']['fot_tipo']);
			imagejpeg($foto, '', 95);
			//$this->render(false);
		}
		
		public function view2($id = null, $size = 'm') {
			
			$this->layout = 'image';
			
			$image = @imagecreatefromjpeg('http://apaeminas.org.br/foto.phtml/'.$id.'/'.$size);
			if (!$image) {
				$image = @imagecreatefromgif('http://apaeminas.org.br/foto.phtml/'.$id.'/'.$size);				
			}
			if (!$image) {
				$image = @imagecreatefrompng('http://apaeminas.org.br/foto.phtml/'.$id.'/'.$size);				
			}
			header('Content-type:image/png');
			
			echo imagejpeg($image);
			
			$this->render(false);
			
		}


		
	}
