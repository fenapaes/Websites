<?php
	class UsuarioController extends AdminAppController {
		
		public $layout = 'login';
		
		public function login() {
			if ($this->request->is('post')) {
				if ($this->Auth->login()) {
					$this->Session->setFlash('Login realizado com successo');
					$this->redirect('/admin');
				} else {
					$this->Session->setFlash('Erro de Login');
				}
			}
		}
		
		public function logout() {
			$this->Auth->logout();
			$this->Session->setFlash('Você saiu do sistema!');
			$this->redirect('/admin');
		}
		
	}