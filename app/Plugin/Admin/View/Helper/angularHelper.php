<?php
	class angularHelper extends AppHelper {

		var $helpers = array('Html');

		public function scripts() {
			echo $this->Html->script('/admin/bower_components/angular/angular.min.js');
			echo $this->Html->script('/admin/bower_components/angular-classy/angular-classy.min.js');
			echo $this->Html->script('/admin/bower_components/angular-route/angular-route.min.js');
			echo $this->Html->script('/admin/bower_components/angular-resource/angular-resource.min.js');
			echo $this->Html->script('/admin/bower_components/angular-animate/angular-animate.min.js');
			echo $this->Html->script('/admin/bower_components/angular-ui-bootstrap-bower/ui-bootstrap-tpls.min.js');
			echo $this->Html->script('/admin/bower_components/ng-sortable/dist/ng-sortable.min.js');
			echo $this->Html->script('//maps.googleapis.com/maps/api/js');
			echo $this->Html->script('/admin/bower_components/ng-maps/dist/ng-maps.min.js');
		}

		public function controllers() {
			echo $this->Html->script('Admin./app/app.js');
			echo $this->Html->script('Admin./app/routes.js');
			echo $this->Html->script('Admin./app/dataService.js');
			echo $this->Html->script('Admin./app/modelService.js');
			// App Controllers
			echo $this->Html->script('/admin/app/Entidade/index.js');
			echo $this->Html->script('/admin/app/mainController.js');
			echo $this->Html->script('/admin/app/Home/controller.js');
			echo $this->Html->script('/admin/app/Menu/index.js');
			echo $this->Html->script('/admin/app/Noticia/index.js');
			echo $this->Html->script('/admin/app/Noticia/form.js');
			echo $this->Html->script('/admin/app/Estado/index.js');
			echo $this->Html->script('/admin/app/Estado/form.js');
			echo $this->Html->script('/admin/app/Arquivo/controller.js');
			echo $this->Html->script('/admin/app/ArquivoTop/controller.js');
			echo $this->Html->script('/admin/app/Banner/controller.js');
		}

	}
