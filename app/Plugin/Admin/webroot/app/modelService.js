sitesApp.service('ModelService', ['$resource','DataService', function($resource, DataService) {
	this.model = {
		Noticia: {
			res: $resource('/api/noticia/:id.json'),
			params: {
				q: 'Noticia.not_ent_id.eq.'+DataService.Entidade.id,
				sort: '-not_data'
			},
			page: 1,
			get: function() {
				return this.model.Noticia.res.get(this.model.Noticia.params).$promise;
			}.bind(this),
		},
		Pagina: {
			res: $resource('/api/pagina/:id.json'),
		},
		Estado: {
			res: $resource('/api/estado/:id.json'),
			params: {
				sort: 'sigla',
				limit: 100
			},
			get: function(params) {
				return this.model.Estado.res.get((params)?params:this.model.Estado.params).$promise;
			}.bind(this),
			save: function(id, data) {
				this.model.Estado.res.save({id:id},data);
			}.bind(this)
		},

		Entidade: {
			res: $resource('/api/entidade/:id.json'),
			params: {
				sort: 'ent_nome',
				page: 1
			},
			get: function(params) {
				return this.model.Entidade.res.get((params)?params:this.model.Entidade.params).$promise;
			}.bind(this),
			save: function(id, data) {
				this.model.Entidade.res.save({id:id},data);
			}.bind(this)
		},

		Menu: {
			res: $resource('/api/menu/:id.json'),
			save: function(id, data) {
				this.model.Menu.res.save({id:id},data);
			}.bind(this)
		},
		Arquivo: {
			res: $resource('/api/arquivo/:id.json'),
		},
		Banner: {
			res: $resource('/api/banner/:id.json'),
		},
		Foto: {
			res: $resource('/api/foto/:id.json'),
		},
		Bootswatch: {
			res: $resource('http://api.bootswatch.com/3/'),
			get: function() {
				return this.model.Bootswatch.res.get().$promise;
			}.bind(this),
		},
	};

}]);
