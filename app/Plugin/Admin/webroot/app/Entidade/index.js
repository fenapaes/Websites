sitesApp.cC({
	name: 'EntidadeCtrl',
	inject: ['$scope','ModelService','DataService','$location'],
	init: function() {
		this.$.loading = false;
		this.$.norecords = false;
		// Pra facilitar
		this.Entidade = this.ModelService.model.Entidade;
		// Paginação
		this.$.pagination = {
			page: this.ModelService.model.Entidade.params.page,
			limit: 10,
			count: 0,
			pageCount: 1
		};
		this.$.entidadeSearch = '';
		this.$.situacoesEntidade = {
			a: 'Apae',
			i: 'Fenderação',
			b: 'Candidata',
		};

		// Método ao iniciar
		this._load();
	},
	watch: {
		// Observando mudança de página
		'{object}pagination.page':'_reload'
	},
	methods: {
		_reload: function(newv, oldv) {
			// Carrega nova página apenas se o valor novo for diferente do valor antigo
			if (newv != oldv) {
				this.Entidade.params.page = newv;
				this._load();
			}
		},
		_load: function() {
			this.$.loading = true;
			// Ler as noticias do banco através do ModelService
			// Todo: Tem uma forma melhor, sem usar o .then ?
			this.Entidade.get(
				{
					q: 'Entidade.ent_nome.lk.'+this.$.entidadeSearch,
					page: this.Entidade.params.page,
					sort: 'ent_fed_uf,ent_nome'
				}
			).then(function(data){
				this.$.data = data.data;
				this.$.pagination = data.pagination;
				this.$.loading = false;
				if (data.data.length == 0) {
					this.$.norecords = true;
				}
			}.bind(this));
		},
		search: function() {
			this.Entidade.params.page = 1;
			this._load();
		},
		searchCancel: function() {
			this.$.entidadeSearch = '',
			this.Entidade.params.page = 1;
			this._load();
		},
		add: function() {
			this.$location.path('entidades/add');
		},
		edit: function(item) {
			this.$location.path('entidades/edit/'+item.Entidade.ent_id);
		},
    image: function(item) {
      this.$location.path('entidades/image/'+item.Entidade.ent_id);
    },
	}
});
