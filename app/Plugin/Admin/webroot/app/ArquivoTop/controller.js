sitesApp.cC({
	name: 'arquivoTopCtrl',
	inject: ['$scope','$resource'],
	init: function() {
		this.ArquivoTop = this.$resource('/api/arquivotop/:id.json');
		this._load();
	},
	methods: {
		_load: function() {
			this.ArquivoTop.get(
				{
					q: 'ArquivoTop.art_ent_id.eq.1'
				}
			).$promise
			.then(function(data){
				this.$.ArquivosTop = data.data;
			}.bind(this));
		}
	}
});
