sitesApp.config(function($routeProvider) {
	$routeProvider
	.when('/', {
		controller: 'homeCtrl',
		templateUrl: '/admin/app/Home/index.html'
	})

	.when('/entidades', {
		controller: 'EntidadeCtrl',
		templateUrl: '/admin/app/Entidade/index.html'
	})
	.when('/entidades/add', {
		controller: 'EntidadeFormCtrl',
		templateUrl: '/admin/app/Entidade/form.html'
	})
	.when('/entidades/edit/:id', {
		controller: 'EntidadeFormCtrl',
		templateUrl: '/admin/app/Entidade/form.html'
	})
	.when('/entidades/image/:id', {
		controller: 'EntidadeImageCtrl',
		templateUrl: '/admin/app/Entidade/image.html'
	})

	.when('/menus', {
		controller: 'menuCtrl',
		templateUrl: '/admin/app/Menu/index.html'
	})
	.when('/noticias', {
		controller: 'noticiaCtrl',
		templateUrl: '/admin/app/Noticia/index.html'
	})
	.when('/noticias/add', {
		controller: 'noticiaFormCtrl',
		templateUrl: '/admin/app/Noticia/form.html'
	})
	.when('/noticias/edit/:not_id', {
		controller: 'noticiaFormCtrl',
		templateUrl: '/admin/app/Noticia/form.html'
	})
	.when('/estados', {
		controller: 'estadoCtrl',
		templateUrl: '/admin/app/Estado/index.html'
	})
	.when('/estados/add', {
		controller: 'estadoFormCtrl',
		templateUrl: '/admin/app/Estado/form.html'
	})
	.when('/estados/edit/:id', {
		controller: 'estadoFormCtrl',
		templateUrl: '/admin/app/Estado/form.html'
	})
	.when('/paginas', {
		controller: 'paginaCtrl',
		templateUrl: '/admin/app/Pagina/index.html'
	})
	.when('/arquivos', {
		controller: 'arquivoCtrl',
		templateUrl: '/admin/app/Arquivo/index.html'
	})
	.when('/arquivos/add', {
		controller: 'arquivoFormCtrl',
		templateUrl: '/admin/app/Arquivo/form.html'
	})
	.when('/arquivostop', {
		controller: 'arquivoTopCtrl',
		templateUrl: '/admin/app/ArquivoTop/index.html'
	})
	.when('/banners', {
		controller: 'bannerCtrl',
		templateUrl: '/admin/app/Banner/index.html'
	})
	.when('/fotos', {
		controller: 'fotoCtrl',
		templateUrl: '/admin/app/Foto/index.html'
	})
	.otherwise({
		templateUrl: '/admin/app/errors/404.html'
	});

});
