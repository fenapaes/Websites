<?php

	Router::resourceMap( array(
		array( 'action' => 'index', 'method' => 'GET', 'id' => false ),
		array( 'action' => 'view', 'method' => 'GET', 'id' => true ),
		array( 'action' => 'add', 'method' => 'POST', 'id' => false),
		array( 'action' => 'edit', 'method' => 'POST', 'id' => true ),
		array( 'action' => 'delete', 'method' => 'DELETE', 'id' => true ),
	) );


	Router::mapResources(
		array(
			'Api.Noticia',
			'Api.Artigo',
			'Api.Arquivo',
			'Api.Menu',
			'Api.Estado',
			'Api.Entidade'
		)
	);

	Router::parseExtensions();
