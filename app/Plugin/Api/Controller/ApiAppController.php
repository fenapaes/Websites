<?php
class ApiAppController extends AppController {
	
	public $components = array( 'RequestHandler' );
	
	public function beforeFilter() {
		$this->layout = 'ajax';
	}
	
	public function _operators($oper) {
	
		$opers = array(
			'eq'=>'',
			'lt'=>'<',
			'gt'=>'>',
			'nq'=>'!=',
			'lk'=>'like',
			'nu'=>'IS NULL',
			'in'=>''
		);
		return $opers[$oper];
	}
	
	public function _query($wheres = '') {
		if ($wheres == '') return array();
		$result = array();
		$wheres = split(',', $wheres);
		
		foreach($wheres as $where) {
			
			$parts = split('\.', $where);
			if ($parts[2] == 'nu' || $parts == 'nn') {
				array_push($result, 
					$parts[0].'.'.$parts[1].' '.$this->_operators($parts[2])
				);
			} else if ($parts[2] == 'lk') {
				array_push($result, 
					array(
						$parts[0].'.'.$parts[1].' '.$this->_operators($parts[2]) => '%'.$parts[3].'%'
					)
				);
			} else if ($parts[2] == 'in') {
				array_push($result, 
					array(
						$parts[0].'.'.$parts[1].' '.$this->_operators($parts[2]) => explode('|', $parts[3])
					)
				);
			} else {
				array_push($result, 
					array(
						$parts[0].'.'.$parts[1].' '.$this->_operators($parts[2]) => $parts[3]
					)
				);
			}
		}

		return $result;
	}
	
	public function _sort($sorts = '') {
		$result = array();
		$sorts = split(',', $sorts);
		foreach($sorts as $sort) {
			$parts = split('-', $sort);
			if (count($parts) == 2) {
				array_push($result, 
					array(
						$this->modelClass.'.'.$parts[1] => 'DESC'
					)
				);
			} else {
				array_push($result, 
					array(
						$this->modelClass.'.'.$parts[0] => 'ASC'
					)
				);
			}
		}
		return $result;
	}

	public function index() {
				
		$query = $this->request->query;
		
		if (!isset($query['page'])) {
			$query['page'] = 1;
		}
		if (!isset($query['limit'])) {
			$query['limit'] = 10;
		}
		if (!isset($query['sort'])) {
			$query['sort'] = array();
		} else {
			$query['sort'] = $this->_sort($query['sort']);
		}
		if (!isset($query['q'])) {
			$query['q'] = '';
		} else {
			$query['q'] = $this->_query($query['q']);
		}
		if (!isset($query['fields'])) {
			$query['fields'] = '';
		}
		
		$offset = ($query['page'] - 1) * $query['limit'];
		
		$pagination = array(	
			'count' => $this->{$this->modelClass}->find(
				'count',
				array(
					'conditions'=>$query['q']
				)
			),
			'limit' => $query['limit'],
			'page' => $query['page'],
		);
		$pagination['pageCount'] = ceil( $pagination['count']/$query['limit'] );
		
		$this->{$this->modelClass}->Behaviors->attach('Containable');		
		if (isset($query['populate'])) {
			$this->{$this->modelClass}->contain( $query['populate']);
		} else {
			$this->{$this->modelClass}->contain();
		}
		
		
		
		$data = $this->{$this->modelClass}->find('all',
			array(
				'limit'=>$query['limit'],
				'offset'=>$offset,
				'order'=>$query['sort'],
				'conditions'=>$query['q']
			)
		);
		
		$this->set('pagination', $pagination);
		$this->set('data', $data);
		$this->set('_serialize', array( 'data', 'pagination' ) );
		
		$this->render(false);
		
	}
	
	public function view($id = null) {
		
		$data = $this->{$this->modelClass}->read(null,$id);
		$this->set('data', $data );
		$this->set('_serialize', array( 'data' ) );
		
		$this->render(false);
		
	}
	
	public function add() {
		
		$data = $this->request->input('json_decode');
		
		$this->{$this->modelClass}->save($data);
		
		$this->set('data', $data );
		$this->set('_serialize', array( 'data' ) );
		
		$this->render(false);
		
	}
	
	public function edit($id = null) {
		
		$data = $this->request->data;
		
		$this->{$this->modelClass}->save($data);
		
		$this->set('data', $data );
		$this->set('_serialize', array( 'data' ) );
		$this->render(false);
		
	}
	
	public function delete($id = null) {
		$this->{$this->modelClass}->delete($id);
		//$this->set('data', $data );
		//$this->set('_serialize', array( 'data' ) );
		$this->render(false);
		
	}
	
}
