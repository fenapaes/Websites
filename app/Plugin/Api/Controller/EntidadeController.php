<?php
class EntidadeController extends ApiAppController {

	public $components = array('RequestHandler');

	public $uses = array('Web.Entidade');

	public function host($host = 'apaebrasil.org.br') {

		$this->Entidade->Behaviors->attach('Containable');
		$this->Entidade->contain();

		$data = $this->Entidade->find('first', array(
			'conditions' => array(
				'Entidade.ent_link' => $host
			)
		));

		$this->set('data', $data);
		$this->set('_serialize', array( 'data' ) );

		$this->render(false);

	}


}
