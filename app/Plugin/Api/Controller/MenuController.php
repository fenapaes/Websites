<?php
class MenuController extends ApiAppController {
	
	public $components = array('RequestHandler');

	public $uses = array('Web.Menu','Web.Menu1','Web.Menu2','Web.Menu3');
	
	public function ajusta() {
		
		$this->Menu1->Behaviors->attach('Containable');
		$this->Menu1->contain('Menu2','Menu2.Menu3');
		
		$menus = $this->Menu1->find('all', array('limit'=>'100'));
		
		pr($menus);

	}
	
	public function menus() {
		
		$query = $this->request->query;
		
		if (!isset($query['ent_id'])) {
			$query['ent_id'] = 1;
		}
		if (!isset($query['men_tipo'])) {
			$query['men_tipo'] = 'h';
		}
		
		$this->Menu->Behaviors->attach('Containable');
		$this->Menu->contain('Submenu1','Submenu1.Submenu2');
				
		$data = $this->Menu->find('all', array(
			'limit' => 100,
			'order' => array('Menu.men_posicao'=>'ASC'),
			'conditions' => array(
				'Menu.men_ent_id' => $query['ent_id'],
				'Menu.men_tipo' => $query['men_tipo'],
				'Menu.men_parent_id IS NULL'
			)
		));
		
		$this->set('data', $data);
		$this->set('_serialize', array( 'data') );
		
		$this->render(false);
		
	}
	
	
}

