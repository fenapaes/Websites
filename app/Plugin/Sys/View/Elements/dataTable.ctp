<?php //pr($data); ?>
<table class="table">
	<thead>
		<tr>
			<th class="col-md-2">&nbsp;</th>
			<?php foreach($fields as $key=>$value) { ?>
			<th><?php echo $value;?></th>
			<?php } ?>
		</tr>
	</thead>
	<tbody>
		<?php foreach ($data as $result) { //pr($result);?>
		<tr>
			<td>
				<?php echo $this->Element('actions', ['id'=>$result[$model]['ent_id']]); ?>
			</td>
			<?php foreach($fields as $key=>$value) { ?>
			<?php $field = explode('.', $key); ?>
			<td><?php echo $result[$field[0]][$field[1]];?></td>
			<?php } ?>
		</tr>
		<?php } ?>
	</tbody>
</table>