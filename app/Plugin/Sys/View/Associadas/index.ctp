<div class="page-header">
	<h3>Entidades</h3>
</div>
<div class="row">
	<div class="col-md-10">
		<ul class="pagination">
			<?php echo $this->Paginator->first('Primeira'); ?>
			<?php echo $this->Paginator->prev('Anterior'); ?>
		</ul>
		<?php echo $this->Paginator->numbers(); ?>
		<ul class="pagination">
			<?php echo $this->Paginator->next('Próxima'); ?>
			<?php echo $this->Paginator->last('Última'); ?>
		</ul>
	</div>
	<div class="col-md-2">
		<div class="pull-right">
			<?php echo $this->Html->link('Adicionar',['action'=>'adicionar'],['class'=>'btn btn-default']); ?>
		</div>
	</div>
</div>
<div class="panel panel-primary">
	<div class="panel-heading">
		<h3 class="panel-title">Entidades</h3>
	</div>
	
	<?php echo $this->Element('dataTable', 
		[
			'data'=>$data, 
			'fields'=>['Associada.ent_nome'=>'Nome','Associada.ent_link'=>'Link'],
			'model' => 'Associada'
		]
	); ?>

</div>

