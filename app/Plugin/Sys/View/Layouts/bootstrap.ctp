<!DOCTYPE html>
<html>
<head>
	<title>SysApae</title>
	<?php
		echo $this->Html->meta('icon');

		echo $this->Html->css('/sys/bower_components/bootstrap/dist/css/bootstrap.min');

		echo $this->fetch('meta');
		echo $this->fetch('css');
		echo $this->fetch('script');
	?>
</head>
<body>
	<div class="container">
		<?php echo $this->Session->flash(); ?>
		<?php echo $this->fetch('content'); ?>
	</div>
	<?php echo $this->element('sql_dump'); ?>
</body>
</html>
