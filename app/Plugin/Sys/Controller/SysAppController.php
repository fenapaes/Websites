<?php
	class SysAppController extends AppController {
		
		public $helpers = array(
			'Html' => array(
				'className' => 'Bootstrap3.BootstrapHtml'
			),
			'Form' => array(
				'className' => 'Bootstrap3.BootstrapForm'
			),
			'Modal' => array(
			 	'className' => 'Bootstrap3.BootstrapModal'
			),
			'Paginator' => array(
			 	'className' => 'Bootstrap3.BootstrapPaginator'
			),
			'Navbar' => array(
			 	'className' => 'Bootstrap3.BootstrapNavbar'
			)
		);
		
		public $layout = 'Sys.bootstrap';
		
		public function beforeRender() {
			
		}
	}