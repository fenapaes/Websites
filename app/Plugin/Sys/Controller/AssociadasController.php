<?php
	class AssociadasController extends SysAppController {
		
		public $components = array('Paginator');
		
		public $uses = ['Sys.Associada'];
		
		public function index() {
			
			$data = $this->Paginator->paginate('Associada');
			
			$this->set('data', $data);
			
		}
		
	}